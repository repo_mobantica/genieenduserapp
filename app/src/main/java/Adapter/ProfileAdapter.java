package Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.genieiot.gsmarthome.AddNewTask;
import com.genieiot.gsmarthome.R;

import java.util.ArrayList;
import java.util.HashMap;

import Fragments.ProfileFragment;

import static Database.DatabaseHandler.ISMODE_SELECTED;
import static Database.DatabaseHandler.MODE_NAME;
import static Database.DatabaseHandler.SWITCH_NAME;

/**
 * Created by root on 17/3/17.
 */

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.MyViewHolder>
{
    Context context;
    ArrayList<HashMap<String, String>> arrayList=new ArrayList<>();
    int mPosition;
    String mRoomName;
    ProfileFragment thisFragment;


    public ProfileAdapter(Context context, ArrayList<HashMap<String, String>> arrayList, ProfileFragment profileFragment) {
        this.context=context;
        this.arrayList=arrayList;
        thisFragment=profileFragment;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.profile_row_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position){
      final String modeName=arrayList.get(position).get(MODE_NAME);
      final String isSelectedMode=arrayList.get(position).get(ISMODE_SELECTED);



      holder.tvMode.setText(modeName);

        if(modeName.equals("Add New Mode")){
            holder.imgAdd.setVisibility(View.VISIBLE);
            //holder.tvMode.setGravity(Gravity.CENTER);
            //holder.tvMode.setCompoundDrawablesWithIntrinsicBounds(R.drawable.add_mode_icon,0,0,0);
            holder.tvMode.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            //holder.tvMode.setTypeface(null, Typeface.BOLD);;
            holder.tvActive.setVisibility(View.GONE);
        }else{
            holder.tvActive.setVisibility(View.VISIBLE);
            holder.imgAdd.setVisibility(View.GONE);
        }

        if(isSelectedMode.equals("1")){
            holder.tvActive.setVisibility(View.VISIBLE);
        }else{
            holder.tvActive.setVisibility(View.GONE);
        }


        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    thisFragment.onClickListner(arrayList.get(position));
            }
        });

        holder.linear.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(!modeName.equals("Add New Mode")) {
                    thisFragment.OnLongClickListener(arrayList.get(position));
                }
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvMode;
        TextView tvActive;
        LinearLayout linear;
        ImageView imgAdd;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvMode= (TextView) itemView.findViewById(R.id.tvMode);
            tvActive= (TextView) itemView.findViewById(R.id.tvActive);
            linear=(LinearLayout)itemView.findViewById(R.id.linear);
            imgAdd=(ImageView) itemView.findViewById(R.id.imgAdd);
        }
    }

}
