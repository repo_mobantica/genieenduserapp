package Adapter;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.genieiot.gsmarthome.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import Database.DatabaseHandler;
import Fragments.Recent;
import Session.Constants;
import Session.SessionManager;

import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_IMAGE_ID;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.TIME;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;

/**
 * Created by root on 4/1/17.
 */

public class AdapterCustomRecent extends RecyclerView.Adapter<AdapterCustomRecent.MyViewHolder> {

    private static final String TABLE_SWITCHES ="Switches" ;
    private ArrayList<HashMap<String, String>> switchDatas;
    String[] result;
    Context context;
    int[] imageId;
    DatabaseHandler db;
    private static LayoutInflater inflater = null;
    private boolean flag = false;
    String roomid = null;
    ArrayList<String> cnt = new ArrayList<String>();
    private boolean[] mCheckedState;
    SessionManager sp;
    LinearLayout llLinear;
    public static int onSwitchCount = 0;
    ArrayList<HashMap<String, Integer>> switchONOFFImage;
    Recent thisInstance;

    public AdapterCustomRecent(Context context, ArrayList<HashMap<String, String>> switchDatas,ArrayList<HashMap<String, Integer>> switchONOFFImage,Recent recent) {

        sp = new SessionManager(context);
        flag = false;
        this.switchDatas = switchDatas;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        db = new DatabaseHandler(context);
        mCheckedState = new boolean[switchDatas.size()];
        this.switchONOFFImage=switchONOFFImage;
        onSwitchCount = 0;
        thisInstance=recent;
    }

    public void updateData(ArrayList<HashMap<String, String>> arrayList){
        this.switchDatas=arrayList;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recent, parent, false);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        holder.switchSwitch.setOnCheckedChangeListener(null);
        holder.tvSwitcName.setText(switchDatas.get(position).get(SWITCH_NAME));
        holder.tvRoomName.setText(switchDatas.get(position).get(ROOM_NAME));
        String mTimeTag=calulateRecentTime(switchDatas.get(position).get(TIME));
        holder.txtTime.setText(mTimeTag);

        if (switchDatas.get(position).get(SWITCH_STATUS).equals("0"))
        {
            holder.switchSwitch.setChecked(false);
            //holder.switchSwitch.setThumbResource(R.drawable.thumb_image1);
            holder.img.setImageResource(switchONOFFImage.get(Integer.parseInt(switchDatas.get(position).get(SWITCH_IMAGE_ID))).get("OFF"));

        }
        else
        {
            holder.switchSwitch.setChecked(true);
            //holder.switchSwitch.setThumbResource(R.drawable.thumb_image);
            holder.img.setImageResource(switchONOFFImage.get(Integer.parseInt(switchDatas.get(position).get(SWITCH_IMAGE_ID))).get("ON"));
        }

        holder.switchSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(holder.switchSwitch.isChecked()){
                    if(sp.getDemoUser().equals("DemoUser")) {
                       // String strConnection = ":!200:!245:!" + switchDatas.get(position).get(SWITCH_TYPE_ID) + ":!1:!0:!168";
                        //new LongOperation().execute(strConnection,switchDatas.get(position).get(IP));
                        HashMap<String,String> mMap=switchDatas.get(position);
                        mMap.put(SWITCH_STATUS,"1");
                        switchDatas.set(position,mMap);
                        db.updateSwitchStatus(mMap.get(SWITCH_ID),"1",switchDatas.get(position).get(DIMMER_VALUE));
                        db.updateRecentSwitchStatus(mMap.get(SWITCH_ID),"1",switchDatas.get(position).get(DIMMER_VALUE));
                        switchDatas.set(position,mMap);
                        thisInstance.OnClickOfSwitchChange(mMap);

                    }else{


                        holder.img.setImageResource(switchONOFFImage.get(Integer.parseInt(switchDatas.get(position).get(SWITCH_IMAGE_ID))).get("ON"));
                        //holder.switchSwitch.setThumbResource(R.drawable.thumb_image);
                       // new SwitchOnOffAsyncTask().execute(position + "", "0", "1");

                        HashMap<String,String> mMap=switchDatas.get(position);
                        mMap.put(SWITCH_STATUS,"1");
                        switchDatas.set(position,mMap);
                      //  db.updateSwitchStatus(mMap.get(SWITCH_ID),"1",switchDatas.get(position).get(DIMMER_VALUE));
                       // db.updateRecentSwitchStatus(mMap.get(SWITCH_ID),"1",switchDatas.get(position).get(DIMMER_VALUE));
                        switchDatas.set(position,mMap);
                        thisInstance.OnClickOfRecentSwitchChange(mMap);


                        onSwitchCount = onSwitchCount + 1;

                    }

                }else{
                    if(sp.getDemoUser().equals("DemoUser")){
                        //String strConnection = ":!200:!245:!" + switchDatas.get(position).get(SWITCH_TYPE_ID) + ":!0:!0:!168";
                        //new LongOperation().execute(strConnection,switchDatas.get(position).get(IP));

                        HashMap<String,String> mMap=switchDatas.get(position);
                        mMap.put(SWITCH_STATUS,"0");
                        Log.d("Switch Info",""+mMap);
                        db.updateSwitchStatus(mMap.get(SWITCH_ID),"0",switchDatas.get(position).get(DIMMER_VALUE));
                        db.updateRecentSwitchStatus(mMap.get(SWITCH_ID),"0",switchDatas.get(position).get(DIMMER_VALUE));
                        switchDatas.set(position,mMap);
                        thisInstance.OnClickOfSwitchChange(mMap);
                        notifyItemChanged(position);

                    }else {

                        onSwitchCount = onSwitchCount - 1;
                        holder.img.setImageResource(switchONOFFImage.get(Integer.parseInt(switchDatas.get(position).get(SWITCH_IMAGE_ID))).get("OFF"));
                        //holder.switchSwitch.setThumbResource(R.drawable.thumb_image1);
                     //  new SwitchOnOffAsyncTask().execute(position + "", "0", "0");

                        HashMap<String,String> mMap=switchDatas.get(position);
                        mMap.put(SWITCH_STATUS,"0");
                        Log.d("Switch Info",""+mMap);
                      //  db.updateSwitchStatus(mMap.get(SWITCH_ID),"0",switchDatas.get(position).get(DIMMER_VALUE));
                      //  db.updateRecentSwitchStatus(mMap.get(SWITCH_ID),"0",switchDatas.get(position).get(DIMMER_VALUE));
                        switchDatas.set(position,mMap);
                        thisInstance.OnClickOfRecentSwitchChange(mMap);
                        notifyItemChanged(position);

                    }

                }
            }
        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return switchDatas.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvSwitcName,tvRoomName,txtTime,txtRangeValue;
        ImageView img;
        SwitchCompat switchSwitch;
        LinearLayout  llLinear;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvSwitcName = (TextView) itemView.findViewById(R.id.txtItemSwitch);
            llLinear= (LinearLayout) itemView.findViewById(R.id.llLinear);
            txtRangeValue = (TextView) itemView.findViewById(R.id.txtRange);
            img = (ImageView) itemView.findViewById(R.id.imgItemSwitch);
            switchSwitch = (SwitchCompat) itemView.findViewById(R.id.switchSwitch);
            tvRoomName= (TextView) itemView.findViewById(R.id.txtItemRoom);
            txtTime= (TextView) itemView.findViewById(R.id.txtTime);

        }
    }

    private String calulateRecentTime(String startDate) {
        String compareString="";
        Date d1 = null;
        Date d2 = null;
        Calendar calander = Calendar.getInstance();
        SimpleDateFormat formatS = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        String stopDate= formatS.format(calander.getTime());
        try{

            d1 = formatS.parse(startDate);
            d2 = formatS.parse(stopDate);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            if(diffDays>1){
                return diffDays+"day ago";
            }else if(diffHours>1){
                return diffHours+" hours "+diffMinutes+" min ago";
            }else if(diffMinutes>1){
                return diffMinutes+" min ago";
            }else {
                return diffSeconds+" sec ago";
            }

        }catch(Exception e){}
        return compareString;
    }

    class SwitchOnOffAsyncTask extends AsyncTask<String,Void,String> {
        int curPosition;
        @Override
        protected String doInBackground(String... params) {
            Constants request=new Constants();
            String messageType="";
            String mResponse=null;

            if(URL_GENIE.equals(URL_GENIE_AWS)){
                messageType=INTERNET;
            }else{
                messageType=LOCAL_HUB;
            }

            try {
                curPosition = Integer.parseInt(params[0]);
                String dimmerStatus=params[1];
                JSONObject jMain= new JSONObject();
                jMain.put("switchId",switchDatas.get(curPosition).get(SWITCH_ID));
                jMain.put("switchStatus",params[2]);
                jMain.put("dimmerValue",switchDatas.get(curPosition).get(DIMMER_VALUE));
                jMain.put("userid", sp.getUSERID());
                jMain.put("messageFrom",messageType);
                Log.d("Recent JSON BODY :",jMain+"");
                mResponse=request.doPostRequest(URL_GENIE+"/switch/changestatus",jMain.toString(),sp.getSecurityToken());
                return mResponse;

            }catch(Exception e){}

            return mResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result!=null) {
                try {
                    JSONObject jResult=new JSONObject(result);
                    Log.d("Change Status Result : ",result);
                    if(jResult.getString("status").equals("SUCCESS")){
                        JSONObject jData=new JSONObject(jResult.getString("result"));
                        HashMap<String,String> mMap=switchDatas.get(curPosition);
                        mMap.put(SWITCH_STATUS,jData.getString("state"));
                        Log.d("Switch Info",""+mMap);
                        db.updateSwitchStatus(mMap.get(SWITCH_ID),jData.getString("state"),jData.getString("dimmerValue"));
                        db.updateRecentSwitchStatus(mMap.get(SWITCH_ID),jData.getString("state"),jData.getString("dimmerValue"));
                        switchDatas.set(curPosition,mMap);
                        //notifyItemChanged(curPosition);
                    }else{
                        Log.d("Change Status Result","Fail");
                    }

                } catch (Exception e) {
                }
            }else{
                Log.d("ChangeStatus","Result null");
            }
        }
    }

}
