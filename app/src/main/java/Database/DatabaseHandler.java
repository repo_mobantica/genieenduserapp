package Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import static android.os.Build.ID;

/**
 * Created by root on 9/26/16.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 4;

    // Database Name
    private static final String DATABASE_NAME = "smartHome";

    // Contacts table name
    private static final String TABLE_ROOM_TYPE = "RoomType";
    // Contacts table name
    private static final String TABLE_SWITCH_TYPE = "SwitchType";
    private static final String TABLE_ROOMS = "Rooms";
    private static final String TABLE_DATETIME = "DateTimer";
    private static final String TABLE_SWITCHES = "Switches";
    private static final String TABLE_RECENT = "Recent";
    private static final String TABLE_NOTIFICATION = "NotificationInfo";
    public static final String NOTIFICATION_MSG="Notify_Message";
    public static final String NOTIFICATION_READ_FLAG="ReadFlag";
    public static final String NOTI_DATE_TIME="Noti_date_time";
    // Contacts Table Columns names
    public static final String ROOM_TYPE_ID = "RoomTypeId";
    public static final String ROOM_IMAGE_TYPE = "RoomImageType";
    public static final String ROOM_TYPE = "RoomType";
    // Contacts Table Columns names
    public static final String ROOM_ID = "RoomId";
    public static final String SCHEDULE_DATETIME = "Schedule_dates";
    public static final String ROOM_NAME = "RoomName";
    public static final String OPERATION = "Operation";
    public static final String SWITCH_TYPE_ID = "SwitchTypeid";
    public static final String SWITCH_TYPE = "SwitchType";
    public static final String SWITCH_IMAGE_TYPE = "SwitchImageType";
    public static final String SWITCH_NAME = "SwitchName";
    public static final String SWITCH_NUMBER = "switchNumber";
    public static final String PANEL_NAME = "panelname";



    public static final String SWITCH_ID = "SwitchID";
    public static final String SWITCH_STATUS = "SwitchStatus";
    public static final String DIMMER_STATUS = "DimmerStatus";
    public static final String DIMMER_VALUE = "DimmerValue";
    public static final String OPERATION_TIME = "OperationTime";
    public static final String RECENT_ID = "RecentID";
    public static final String USER_IMAGE = "UserImage";
    public static final String LOCK ="lock";
    public static final String HIDE ="hide";
    public static final String ROOM_IMAGE_URL ="RoomImageUrl";
    public static final String IMAGE_ON ="onImage";
    public static final String PANEL_ID ="panelId";
    public static final String IMAGE_OFF ="offImage";
    public  static  final String TIME="Time";
    public  static  final String SCHEDULE_ID="schedule_id";
    public  static  final String SELECTED_SWITCH="selected_switch";
    public  static  final String ACTIVITY_TIME="activity_time";
    public  static  final String ACTIVITY_ID="activity_id";
    public  static  final String IPAddress="IpAddress";
    public  static  final String IP="IP";
    public static final String SWITCH_IMAGE_ID = "switch_image_id";
    public  static  final String CREATED_BY="Createdby";
    public final Context context;
    private static final String TAG = "DatabaseHandler";

    private static final String MODE_TABLE="Mode_Table";
    public static final String MODE_ID="_id";
    public static final String MODE_NAME="Mode_Name";
    public static final String ON_MODE="on_mode";
    public static final String OFF_MODE="off_mode";
    public static final String REPEATE="Repeate";
    public static final String ISMODE_SELECTED="isSelected";
    public static final String ISSHORCUT="is_shorcut";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        createTableRoomType(db);
        createTableSwitchType(db);
        createTableAddRoom(db);
        createTableAddSwitch(db);
        createTableRecent(db);
        createTableNotification(db);
        createTableAddDate(db);
        createIpAddress(db);
        createTableMode(db);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            Log.d("Database Handler ", "onUpgrade()");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROOMS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_SWITCHES);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_DATETIME);
            db.execSQL("DROP TABLE IF EXISTS " + MODE_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECENT);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATION);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROOM_TYPE);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_SWITCH_TYPE);
            db.execSQL("DROP TABLE IF EXISTS " + IPAddress);
            onCreate(db);

    }

    private void createTableMode(SQLiteDatabase db) {

        String CREATE_SWITCH_TYPE = "CREATE TABLE " + MODE_TABLE+ "("
                + MODE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + MODE_NAME + " TEXT ," + ON_MODE +" TEXT ,"+OFF_MODE+" TEXT ,"+REPEATE +" TEXT ,"
                + ISMODE_SELECTED +" TEXT DEFAULT 0 ,"+ISSHORCUT+" TEXT DEFAULT 0"+")";
        db.execSQL(CREATE_SWITCH_TYPE);
    }

    private void createTableAddSwitch(SQLiteDatabase db) {

        String sqlQuery = "CREATE TABLE " + TABLE_SWITCHES + "("
                + SWITCH_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + SWITCH_NAME + " TEXT" + "," + SWITCH_TYPE_ID + " TEXT" + ", "
                + SWITCH_STATUS + " TEXT " + ", " + ROOM_ID + " TEXT " + ", " + DIMMER_STATUS + " TEXT " + ", " + SWITCH_NUMBER + " TEXT " + ", " + PANEL_NAME + " TEXT "
                + ", " + DIMMER_VALUE + " TEXT " + ", " + LOCK + " TEXT " + ","
                + HIDE + " TEXT " + "," + PANEL_ID + " TEXT " + ", " + ROOM_NAME + " TEXT " + "," + IMAGE_ON + " TEXT, " +IMAGE_OFF+" TEXT ,"+ SWITCH_IMAGE_ID +" TEXT DEFAULT 0 ,"+ SELECTED_SWITCH+ " TEXT DEFAULT 1 " +")";
        db.execSQL(sqlQuery);
    }
    private void createTableRecent(SQLiteDatabase db) {
        String sqlQuery = "CREATE TABLE " + TABLE_RECENT + "("
                + RECENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + SWITCH_ID + " TEXT" + "," + SWITCH_NAME
                + " TEXT" + "," + SWITCH_TYPE_ID + " TEXT" + ", " + SWITCH_STATUS + " TEXT " + ", " + ROOM_ID
                + " TEXT " + ", " + DIMMER_STATUS + " TEXT " + ", " + DIMMER_VALUE + " TEXT " + ", " + ROOM_NAME
                + " TEXT " + ", " + TIME + " TEXT ," + CREATED_BY +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"
                + SWITCH_IMAGE_ID+" TEXT ,"+IP+" TEXT "+")";
        db.execSQL(sqlQuery);
    }
    private void createTableSwitchType(SQLiteDatabase db) {

        String CREATE_SWITCH_TYPE = "CREATE TABLE " + TABLE_SWITCH_TYPE + "("
                + SWITCH_TYPE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + SWITCH_TYPE + " TEXT" + ")";
        db.execSQL(CREATE_SWITCH_TYPE);
    }
    private void createTableRoomType(SQLiteDatabase db) {

        String CREATE_ROOM_TYPE = "CREATE TABLE " + TABLE_ROOM_TYPE + "("
                + ROOM_TYPE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + ROOM_TYPE + " TEXT" + ")";
        db.execSQL(CREATE_ROOM_TYPE);
    }
    private void createTableAddRoom(SQLiteDatabase db) {
        String sqlQuery = "CREATE TABLE " + TABLE_ROOMS + "("
                + ROOM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + ROOM_NAME + " TEXT " + "," + ROOM_TYPE_ID + " TEXT , "
                + ROOM_IMAGE_URL+" TEXT, "+ ROOM_IMAGE_TYPE +" TEXT DEFAULT 0, "+HIDE+" TEXT DEFAULT 0 "+")";
        db.execSQL(sqlQuery);
    }

    //create table for schedule date
    private void createTableAddDate(SQLiteDatabase db) {

        String sqlQuery = "CREATE TABLE " + TABLE_DATETIME + "("
                +"_id INTEGER PRIMARY KEY AUTOINCREMENT,"+SWITCH_ID+" TEXT, "
                + SWITCH_NAME + " TEXT," + SWITCH_STATUS + " TEXT " + "," + SCHEDULE_DATETIME + " TEXT , "
                + TIME+" TEXT"+ "," + ROOM_NAME+" TEXT ,"+ IP+" TEXT ,"+SCHEDULE_ID+" TEXT ,"+DIMMER_STATUS+" TEXT,"+DIMMER_VALUE+" TEXT"+")";
        db.execSQL(sqlQuery);
    }

    private void createIpAddress(SQLiteDatabase db) {
        String sqlquery = "CREATE TABLE " + IPAddress + "(" + IP+" TEXT "+")";
        db.execSQL(sqlquery);
    }

    private void createTableNotification(SQLiteDatabase db) {
        String CREATE_ROOM_TYPE = "CREATE TABLE " + TABLE_NOTIFICATION + "("
                +"_id INTEGER PRIMARY KEY AUTOINCREMENT," + NOTIFICATION_MSG + " TEXT ," + NOTIFICATION_READ_FLAG+" TEXT ,"+NOTI_DATE_TIME+" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+ACTIVITY_TIME+" TEXT ,"+ACTIVITY_ID+" TEXT, "+USER_IMAGE+" TEXT, "+ROOM_NAME+" TEXT "+")";
        db.execSQL(CREATE_ROOM_TYPE);
    }

    public void insertIpAddress(HashMap<String, String> Ipaddress) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(IP, Ipaddress.get("IpAddress")); // Room Type Name
        // Inserting Row
        db.insert(IPAddress, null, values);
    }
    public ArrayList<HashMap<String, String>> getIP() {

        ArrayList<HashMap<String, String>> listSchedule = new ArrayList<HashMap<String, String>>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + IPAddress ;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToNext()) {
            do {
                HashMap<String, String> mMap = new HashMap<String, String>();
                mMap.put(IP, (cursor.getString(cursor.getColumnIndex(SWITCH_NAME))));

                // Adding contact to list
                listSchedule.add(mMap);
            } while (cursor.moveToNext());
        }

        Log.d("Schedule All Data : ",""+listSchedule);
        return listSchedule;
    }

    public void insertSchedulerInfo(HashMap<String, String> switchData) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SWITCH_ID,switchData.get(SWITCH_ID));
        values.put(SWITCH_NAME, switchData.get(SWITCH_NAME)); // Room Type Name
        values.put(SWITCH_STATUS, switchData.get(SWITCH_STATUS));
        values.put(SCHEDULE_DATETIME, switchData.get(SCHEDULE_DATETIME));
        values.put(TIME, switchData.get(TIME));
        values.put(ROOM_NAME,switchData.get(ROOM_NAME));
        values.put(IP,switchData.get(IP));
        values.put(SCHEDULE_ID,switchData.get(SCHEDULE_ID));
        values.put(DIMMER_STATUS,switchData.get(DIMMER_STATUS));
        values.put(DIMMER_VALUE,switchData.get(DIMMER_VALUE));

        String mQuery="DELETE FROM "+TABLE_DATETIME+" WHERE "+SCHEDULE_ID+" NOT IN (SELECT "+SCHEDULE_ID+" FROM "+TABLE_DATETIME+" ORDER BY "+SCHEDULE_ID+" DESC LIMIT 20)";

        Cursor cursor1=db.rawQuery(mQuery,null);
        Log.d("ScheduleList delete",""+cursor1.getCount());

        try {
            // Inserting Row
            db.insert(TABLE_DATETIME, null, values);
            Log.d("Insert Schedule : ","Success "+values);
            db.close();
        } catch (Exception e) {
            Log.d("Insert Schedule : "," Fail");
            e.printStackTrace();
        }
    }

    public ArrayList<HashMap<String, String>> getSchedulerInfo() {

        ArrayList<HashMap<String, String>> listSchedule = new ArrayList<HashMap<String, String>>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_DATETIME ;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToNext()) {
            do {
                HashMap<String, String> mMap = new HashMap<String, String>();
                mMap.put(SWITCH_NAME, (cursor.getString(cursor.getColumnIndex(SWITCH_NAME))));
                mMap.put(SWITCH_ID, (cursor.getString(cursor.getColumnIndex(SWITCH_ID))));
                mMap.put(SWITCH_STATUS, (cursor.getString(cursor.getColumnIndex(SWITCH_STATUS))));
                mMap.put(SCHEDULE_DATETIME, (cursor.getString(cursor.getColumnIndex(SCHEDULE_DATETIME))));
                mMap.put(TIME, (cursor.getString(cursor.getColumnIndex(TIME))));
                mMap.put(ROOM_NAME,(cursor.getString(cursor.getColumnIndex(ROOM_NAME))));
                mMap.put(SCHEDULE_ID,(cursor.getString(cursor.getColumnIndex(SCHEDULE_ID))));
                mMap.put(DIMMER_VALUE,(cursor.getString(cursor.getColumnIndex(DIMMER_VALUE))));
                mMap.put(DIMMER_STATUS,(cursor.getString(cursor.getColumnIndex(DIMMER_STATUS))));
                // Adding contact to list
                listSchedule.add(mMap);
            } while (cursor.moveToNext());
        }

        Log.d("Schedule All Data : ",""+listSchedule);
        return listSchedule;
    }


    // Adding new contact
    public void insertRoomType(HashMap<String, String> roomtype) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(ROOM_TYPE, roomtype.get("roomTypeName")); // Room Type Name
        try {
            // Inserting Row
            db.insert(TABLE_ROOM_TYPE, null, values);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public HashMap<String, String> getRoom(String roomId) {
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ROOMS + " where " + ROOM_ID + " = '" + roomId + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToNext()) {

            HashMap<String, String> room = new HashMap<String, String>();
            room.put(ROOM_NAME, (cursor.getString(1)));
            room.put(ROOM_TYPE_ID, (cursor.getString(2)));

            return room;
        }

        return null;
    }
    public void insertRoom(HashMap<String, String> room) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ROOM_ID,room.get(ROOM_ID));
        values.put(ROOM_NAME, room.get(ROOM_NAME)); // Room Type Name
        values.put(ROOM_TYPE_ID, room.get(ROOM_TYPE_ID)); // Room Type Name
        values.put(ROOM_IMAGE_TYPE, room.get(ROOM_IMAGE_TYPE)); // Room Type Name

        try {
            // Inserting Row
            db.insert(TABLE_ROOMS, null, values);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void insertRoomNew(HashMap<String, String> room) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(ROOM_ID,room.get(ROOM_ID));
        values.put(ROOM_NAME, room.get(ROOM_NAME)); // Room Type Name
        values.put(ROOM_TYPE_ID, room.get(ROOM_TYPE_ID)); // Room Type Name
        values.put(ROOM_IMAGE_TYPE, room.get(ROOM_IMAGE_TYPE)); // Room Type Name

        Cursor cursor = db.rawQuery("select * from "+TABLE_ROOMS+" where "+ROOM_ID+"='"+room.get(ROOM_ID)+"'",null);

        try {
             if(cursor!=null && cursor.getCount()>0){
                 long mStatus=db.update(TABLE_ROOMS, values, ROOM_ID + "=" + room.get(ROOM_ID),null);
                 Log.d("Get RoomInfo","Update data status "+mStatus+" "+room.get(ROOM_ID));
             }else{
               long mStatus=db.insert(TABLE_ROOMS, null, values);
                 Log.d("Get RoomInfo","Insert data status "+mStatus+" "+room.get(ROOM_ID));
             }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            cursor.close();
            db.close();
        }

    }
    public void insertSwitchType(HashMap<String, String> roomtype) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SWITCH_TYPE, roomtype.get("switchTypeName")); // Room Type Name
        // Inserting Row
        db.insert(TABLE_SWITCH_TYPE, null, values);
    }
    public int getRoomTypeCount() {
        String countQuery = "SELECT  * FROM " + TABLE_ROOM_TYPE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }
    public int getSwitchCount() {
        String countQuery = "SELECT  * FROM " + TABLE_SWITCHES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }
    public int isRoomAvailable(String roomName) {
        String countQuery = "SELECT  * FROM " + TABLE_ROOMS + " where " + ROOM_NAME + " = '" + roomName + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }
    public int isRoomAvailableWithId(String roomName, String roomId) {
        String countQuery = "SELECT  * FROM " + TABLE_ROOMS + " where " + ROOM_NAME + " = '" + roomName + "' AND " + ROOM_ID + " <> '" + roomId + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }
    public void updateRoom(HashMap<String, String> room, String roomId) {
        ContentValues values = new ContentValues();
        values.put(ROOM_NAME, room.get(ROOM_NAME));
        values.put(ROOM_TYPE_ID, room.get(ROOM_TYPE_ID));
        values.put(ROOM_IMAGE_URL,room.get(ROOM_IMAGE_URL));
        SQLiteDatabase db = this.getWritableDatabase();
//        String countQuery = "UPDATE " + TABLE_ROOMS + " SET " + ROOM_NAME + " = '" + room.get(ROOM_NAME) + "'," + ROOM_TYPE_ID + " = '" + room.get(ROOM_TYPE_ID) + "'  where " + ROOM_ID + " = '" + roomId + "'";
      //  Cursor cursor = db.rawQuery(countQuery, null);

        db.update(TABLE_ROOMS, values, ROOM_ID + "=" + roomId, null);
        db.close();

    }
    public void updateRoomImage(String roomImage, String roomId) {
        ContentValues values = new ContentValues();

        values.put(ROOM_IMAGE_TYPE,roomImage);
        SQLiteDatabase db = this.getWritableDatabase();
//        String countQuery = "UPDATE " + TABLE_ROOMS + " SET " + ROOM_NAME + " = '" + room.get(ROOM_NAME) + "'," + ROOM_TYPE_ID + " = '" + room.get(ROOM_TYPE_ID) + "'  where " + ROOM_ID + " = '" + roomId + "'";
//        Cursor cursor = db.rawQuery(countQuery, null);

        db.update(TABLE_ROOMS, values, ROOM_ID + "=" + roomId, null);
        db.close();

    }
    public int getSwitchTypeCount() {
        String countQuery = "SELECT  * FROM " + TABLE_SWITCH_TYPE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }
    public int getRoomCount() {
        String countQuery = "SELECT  * FROM " + TABLE_ROOMS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }
    public ArrayList<HashMap<String, String>> getRooms() {

        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_ROOMS+" where "+HIDE+"='0'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
       // cursor.moveToPosition(0);
        // looping through all rows and adding to list
        if (cursor != null){
            if (cursor.moveToNext()) {
                do {
                    HashMap<String, String> roomType = new HashMap<String, String>();
                    roomType.put(ROOM_ID, "" + Integer.parseInt(cursor.getString(0)));
                    roomType.put(ROOM_NAME, (cursor.getString(1)));
                    roomType.put(ROOM_TYPE_ID, (cursor.getString(2)));
                   // roomType.put(ROOM_IMAGE_URL, (cursor.getString(3)));
                    roomType.put(ROOM_IMAGE_TYPE, (cursor.getString(4)));

                    // Adding contact to list
                    list.add(roomType);
                } while (cursor.moveToNext());
            }
            cursor.close();
    }else{

    }

        db.close();

        return list;
    }
    public ArrayList<HashMap<String, String>> getRoomType() {

        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ROOM_TYPE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToNext()) {
            do {
                HashMap<String, String> roomType = new HashMap<String, String>();
                roomType.put(ROOM_TYPE_ID, "" + Integer.parseInt(cursor.getString(0)));
                roomType.put(ROOM_TYPE, (cursor.getString(1)));

                // Adding contact to list
                list.add(roomType);
            } while (cursor.moveToNext());
        }

        return list;
    }
    public void insertSwitch(HashMap<String, String> switchData) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SWITCH_NAME, switchData.get(SWITCH_NAME)); // Room Type Name
        values.put(SWITCH_TYPE_ID, switchData.get(SWITCH_TYPE_ID));
        values.put(SWITCH_STATUS, switchData.get(SWITCH_STATUS));
        values.put(ROOM_ID, switchData.get(ROOM_ID));
        values.put(DIMMER_STATUS, switchData.get(DIMMER_STATUS));
        values.put(DIMMER_VALUE, switchData.get(DIMMER_VALUE));
        values.put(LOCK,switchData.get(LOCK));
        values.put(HIDE,switchData.get(HIDE));
        values.put(SWITCH_IMAGE_ID,switchData.get(SWITCH_IMAGE_ID));
        values.put(ROOM_NAME,switchData.get(ROOM_NAME));
        try {
            // Inserting Row
            db.insert(TABLE_SWITCHES, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();
    }

    public void insertSwitchNew(HashMap<String, String> switchData) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SWITCH_ID,switchData.get(SWITCH_ID));
        values.put(SWITCH_NAME, switchData.get(SWITCH_NAME)); // Room Type Name
        values.put(SWITCH_TYPE_ID, switchData.get(SWITCH_TYPE_ID));
        values.put(SWITCH_STATUS, switchData.get(SWITCH_STATUS));
        values.put(ROOM_ID, switchData.get(ROOM_ID));
        values.put(DIMMER_STATUS, switchData.get(DIMMER_STATUS));
        values.put(DIMMER_VALUE, switchData.get(DIMMER_VALUE));
        values.put(LOCK,switchData.get(LOCK));
        values.put(HIDE,switchData.get(HIDE));
        values.put(ROOM_NAME,switchData.get(ROOM_NAME));
        values.put(SWITCH_IMAGE_ID,switchData.get(SWITCH_IMAGE_ID));

        values.put(SWITCH_NUMBER,switchData.get(SWITCH_NUMBER));
        values.put(PANEL_NAME,switchData.get(PANEL_NAME));

        Cursor cursor = db.rawQuery("select * from "+TABLE_SWITCHES+" where "+ROOM_ID+"='"+switchData.get(ROOM_ID)+"' AND "
                +SWITCH_ID +" ='"+switchData.get(SWITCH_ID)+"' AND "+HIDE+"='0'",null);
        try {
            if(cursor!=null && cursor.getCount()>0){

                long mStatus=db.update(TABLE_SWITCHES, values, ROOM_ID + "='" + switchData.get(ROOM_ID)+"' AND "
                        +SWITCH_ID +" ='"+switchData.get(SWITCH_ID)+"'",null);

                Log.d("Get SwitchInfo : ","Update data status "+mStatus+" "+switchData.get(SWITCH_ID));

            }else{
                long mStatus=db.insert(TABLE_SWITCHES, null, values);
                Log.d("Get SwitchInfo : ","Insert data status "+mStatus+" "+switchData.get(SWITCH_ID));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            cursor.close();
            db.close();
        }

    }
    public ArrayList<HashMap<String, String>> getSwitches(String mRoomId) {

        ArrayList<HashMap<String, String>> listSwitch = new ArrayList<HashMap<String, String>>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SWITCHES + " where " + ROOM_ID + " =" + mRoomId+" AND " +HIDE+"='0'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToNext()) {
            do {
                HashMap<String, String> switchs = new HashMap<String, String>();
                switchs.put(SWITCH_ID, "" + Integer.parseInt(cursor.getString(cursor.getColumnIndex(SWITCH_ID))));
                switchs.put(SWITCH_NAME, (cursor.getString(cursor.getColumnIndex(SWITCH_NAME))));
                switchs.put(SWITCH_TYPE_ID, (cursor.getString(cursor.getColumnIndex(SWITCH_TYPE_ID))));
                switchs.put(SWITCH_STATUS, (cursor.getString(cursor.getColumnIndex(SWITCH_STATUS))));
                switchs.put(ROOM_ID, (cursor.getString(cursor.getColumnIndex(ROOM_ID))));
                switchs.put(DIMMER_STATUS, (cursor.getString(cursor.getColumnIndex(DIMMER_STATUS))));
                switchs.put(DIMMER_VALUE, (cursor.getString(cursor.getColumnIndex(DIMMER_VALUE))));
                switchs.put(LOCK,cursor.getString(cursor.getColumnIndex(LOCK)));
                switchs.put(HIDE,cursor.getString(cursor.getColumnIndex(HIDE)));
                switchs.put(ROOM_NAME,cursor.getString(cursor.getColumnIndex(ROOM_NAME)));
                switchs.put(SWITCH_IMAGE_ID,cursor.getString(cursor.getColumnIndex(SWITCH_IMAGE_ID)));

                switchs.put(SWITCH_NUMBER,cursor.getString(cursor.getColumnIndex(SWITCH_NUMBER)));
                switchs.put(PANEL_NAME,cursor.getString(cursor.getColumnIndex(PANEL_NAME)));
                // Adding contact to list
                listSwitch.add(switchs);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return listSwitch;
    }
    public void updateSwitch(HashMap<String, String> room, String switchId) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SWITCH_NAME, room.get(SWITCH_NAME));
        values.put(SWITCH_TYPE_ID, room.get(SWITCH_TYPE_ID));
        values.put(DIMMER_STATUS, room.get(DIMMER_STATUS));
        if (room.get(DIMMER_STATUS).equals("0")) {
            values.put(DIMMER_VALUE, room.get(DIMMER_VALUE));
        }
        db.update(TABLE_SWITCHES, values, SWITCH_ID + "=" + switchId, null);

        db.close();
    }
    public void updateDimmerValue(String dimmerValue, String switchId) {

        ContentValues values = new ContentValues();
        values.put(DIMMER_VALUE, dimmerValue);
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_SWITCHES, values, SWITCH_ID + "=" + switchId, null);
        db.close();
    }
    public void updateSwitchStatus(String mSwitchID,String mStatus,String mDimmerValue){

        ContentValues values = new ContentValues();
        values.put(SWITCH_STATUS, mStatus);
        values.put(DIMMER_VALUE,mDimmerValue);
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            long m=db.update(TABLE_SWITCHES, values, SWITCH_ID + "=" + mSwitchID, null);
            Log.d("ChangeStatus : ",m+"");
        }catch(Exception e)
        {
            Log.d("ChangeStatus : ",e.toString()+"");
        }

        db.close();
    }
    public void updateAllSwitchStatus(String mSwitchID,String mStatus){

        ContentValues values = new ContentValues();
        values.put(SWITCH_STATUS, mStatus);
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            long m=db.update(TABLE_SWITCHES, values, SWITCH_ID + "=" + mSwitchID, null);
            Log.d("ChangeStatus : ",m+"");
        }catch(Exception e){ Log.d("ChangeStatus : ",e.toString()+"");}

        db.close();
    }

    public void updateAllDimmerSwitchStatus(String mSwitchID,String mStatus){

        ContentValues values = new ContentValues();
        values.put(SWITCH_STATUS, mStatus);
        values.put(DIMMER_VALUE,"38");
        SQLiteDatabase db = this.getWritableDatabase();
        try {

            String[] args = new String[]{mSwitchID, "1","0"};
            long m=db.update(TABLE_SWITCHES, values, SWITCH_ID+"=? AND "+DIMMER_STATUS+"=? AND "+DIMMER_VALUE+"=?", args);
            //long m=db.update(TABLE_SWITCHES, values, SWITCH_ID + "=" + mSwitchID, null);
            Log.d(TAG, "updateAllDimmerSwitchStatus: ");
        }catch(Exception e){ Log.d("ChangeStatus : ",e.toString()+"");}
        db.close();
    }

    public void updateRecentSwitchStatus(String mSwitchID,String mStatus,String mDimmerValue){
        ContentValues values = new ContentValues();
        values.put(SWITCH_STATUS, mStatus);
        values.put(DIMMER_VALUE,mDimmerValue);
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            long m=db.update(TABLE_RECENT, values, SWITCH_ID + "=" + mSwitchID, null);
            Log.d("Update recent data : ",m+"");
        }catch(Exception e){ Log.d("ChangeStatus : ",e.toString()+"");}

        db.close();
    }
    public ArrayList<HashMap<String, String>> getSwitchType() {

        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SWITCH_TYPE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToNext()) {
            do {
                HashMap<String, String> roomType = new HashMap<String, String>();
                roomType.put(SWITCH_TYPE_ID, "" + Integer.parseInt(cursor.getString(0)));
                roomType.put(SWITCH_TYPE, (cursor.getString(1)));

                // Adding contact to list
                list.add(roomType);
                Log.e("aaa",cursor.getString(1));
            } while (cursor.moveToNext());
        }

        return list;
    }
    public String getSwitchType(String SwitchId) {

       String mSwitchType=null;
        String selectQuery = "SELECT  * FROM " + TABLE_SWITCHES +" where "+SWITCH_ID+"='"+SwitchId+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToNext()) {
            do {

                mSwitchType=cursor.getString(cursor.getColumnIndex(SWITCH_TYPE_ID));
                Log.e("SwitchType ID",mSwitchType);
                return mSwitchType;
            } while (cursor.moveToNext());
        }

        return mSwitchType;
    }
    public int isSwitchAvailable(String switchName, String mRoomId) {
        String countQuery = "SELECT  * FROM " + TABLE_SWITCHES + " where " + SWITCH_NAME + " = '" + switchName + "' AND " + ROOM_ID + " = '" + mRoomId + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }
    public int isSwitchAvailableWithId(String switchName, String mRoomId, String switchId) {
        String countQuery = "SELECT  * FROM " + TABLE_SWITCHES + " where " + SWITCH_NAME + " = '" + switchName + "'  AND " + ROOM_ID + " = '" + mRoomId + "' AND " + SWITCH_ID + " <> '" + switchId + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }
    public HashMap<String, String> getSwitch(String switchId) {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SWITCHES + " where " + SWITCH_ID + " = '" + switchId + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToNext()) {

            HashMap<String, String> room = new HashMap<String, String>();
            room.put(SWITCH_NAME, (cursor.getString(cursor.getColumnIndex(SWITCH_NAME))));
            room.put(SWITCH_TYPE_ID, (cursor.getString(cursor.getColumnIndex(SWITCH_TYPE_ID))));
            room.put(SWITCH_STATUS, (cursor.getString(cursor.getColumnIndex(SWITCH_STATUS))));
            room.put(ROOM_ID, (cursor.getString(cursor.getColumnIndex(ROOM_ID))));
            room.put(DIMMER_STATUS, (cursor.getString(cursor.getColumnIndex(DIMMER_STATUS))));
            room.put(DIMMER_VALUE, (cursor.getString(cursor.getColumnIndex(DIMMER_VALUE))));
            room.put(LOCK,(cursor.getString(cursor.getColumnIndex(LOCK))));
            return room;
        }

        cursor.close();
        db.close();
        return null;
    }
//    public void setSwitchStatus(String switchStatus, String switchId) {
//
//        ContentValues values = new ContentValues();
//        values.put(SWITCH_STATUS, switchStatus);
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.update(TABLE_SWITCHES, values, SWITCH_ID + "=" + switchId, null);
//
//
//    }
    public int getSwitchStatusCount() {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SWITCHES + " where " + SWITCH_STATUS + " = '1'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;

    }
    public void insertRecentSwitch(HashMap<String, String> switchData) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SWITCH_ID, switchData.get(SWITCH_ID));
        values.put(SWITCH_NAME, switchData.get(SWITCH_NAME));
        values.put(SWITCH_TYPE_ID, switchData.get(SWITCH_TYPE_ID));
        values.put(SWITCH_STATUS, switchData.get(SWITCH_STATUS));
        values.put(ROOM_ID, switchData.get(ROOM_ID));
        values.put(DIMMER_STATUS, switchData.get(DIMMER_STATUS));
        values.put(DIMMER_VALUE, switchData.get(DIMMER_VALUE));
        values.put(ROOM_NAME, switchData.get(ROOM_NAME));
        values.put(TIME, switchData.get(TIME));
        values.put(CREATED_BY,switchData.get(CREATED_BY));
        values.put(SWITCH_IMAGE_ID,switchData.get(SWITCH_IMAGE_ID));
        values.put(IP,switchData.get(IP));

       String mQuery="select * from "+TABLE_RECENT+" where "+SWITCH_ID+"='"+switchData.get(SWITCH_ID)+"'";
       Cursor cursor = db.rawQuery(mQuery,null);

        try {
            // Inserting Row
            if(cursor!=null && cursor.getCount()>0){
                db.update(TABLE_RECENT, values, SWITCH_ID + "='" + switchData.get(SWITCH_ID)+"'",null);
                Log.d("Update Recent ",""+values);
            }else {
                db.insert(TABLE_RECENT, null, values);
                Log.d("Insert Recent ",""+values);
            }

            cursor.close();
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public ArrayList<HashMap<String, String>> getRecentSwitches() {

        ArrayList<HashMap<String, String>> listSwitch = new ArrayList<HashMap<String, String>>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_RECENT +" ORDER BY datetime("+CREATED_BY+") ASC";
        //+" ORDER BY date("+TIME+") DESC Limit 1";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToNext()) {
            do {
                HashMap<String, String> switchs = new HashMap<String, String>();
                switchs.put(RECENT_ID, "" + Integer.parseInt(cursor.getString(cursor.getColumnIndex(RECENT_ID))));
                switchs.put(SWITCH_ID, (cursor.getString(cursor.getColumnIndex(SWITCH_ID))));
                switchs.put(SWITCH_NAME, (cursor.getString(cursor.getColumnIndex(SWITCH_NAME))));
                switchs.put(SWITCH_TYPE_ID, (cursor.getString(cursor.getColumnIndex(SWITCH_TYPE_ID))));
                switchs.put(SWITCH_STATUS, (cursor.getString(cursor.getColumnIndex(SWITCH_STATUS))));
                switchs.put(ROOM_ID, (cursor.getString(cursor.getColumnIndex(ROOM_ID))));
                switchs.put(DIMMER_STATUS, (cursor.getString(cursor.getColumnIndex(DIMMER_STATUS))));
                switchs.put(DIMMER_VALUE, (cursor.getString(cursor.getColumnIndex(DIMMER_VALUE))));
                switchs.put(ROOM_NAME,(cursor.getString(cursor.getColumnIndex(ROOM_NAME))));
                switchs.put(TIME,(cursor.getString(cursor.getColumnIndex(TIME))));
                switchs.put(CREATED_BY,(cursor.getString(cursor.getColumnIndex(CREATED_BY)))) ;
                switchs.put(SWITCH_IMAGE_ID,(cursor.getString(cursor.getColumnIndex(SWITCH_IMAGE_ID)))) ;
                switchs.put(IP,(cursor.getString(cursor.getColumnIndex(IP)))) ;
                // Adding contact to list
                Log.d("Created date ",""+switchs);
                listSwitch.add(switchs);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return listSwitch;
    }
    public void UpdateLockFlag(String mRoomId, String switchId, String lockFlag) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LOCK, lockFlag);
        long result=db.update(TABLE_SWITCHES, values, SWITCH_ID + "=" + switchId+" AND "+ROOM_ID+"="+mRoomId, null);
        Log.d("Update Lock Status ",lockFlag);
        db.close();

    }
    public void UpdateHideStatus(String mRoomId, String switchId, String hideFlag) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(HIDE, hideFlag);
        long result=db.update(TABLE_SWITCHES, values, SWITCH_ID + "=" + switchId+" AND "+ROOM_ID+"="+mRoomId, null);
        Log.d("Update Hide Status ",""+values);
        db.close();
    }
    public ArrayList<HashMap<String, String>> getHideSwitches(String mRoomId) {

        ArrayList<HashMap<String, String>> listSwitch = new ArrayList<HashMap<String, String>>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SWITCHES + " where " + HIDE + " = '1' AND "+ROOM_ID+"='"+mRoomId+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToNext()) {
            do {
                HashMap<String, String> switchs = new HashMap<String, String>();
                switchs.put(SWITCH_ID, (cursor.getString(cursor.getColumnIndex(SWITCH_ID))));
                switchs.put(SWITCH_NAME, (cursor.getString(cursor.getColumnIndex(SWITCH_NAME))));
                switchs.put(SWITCH_TYPE_ID, (cursor.getString(cursor.getColumnIndex(SWITCH_TYPE_ID))));
                switchs.put(SWITCH_STATUS, (cursor.getString(cursor.getColumnIndex(SWITCH_STATUS))));
                switchs.put(ROOM_ID, (cursor.getString(cursor.getColumnIndex(ROOM_ID))));
                switchs.put(DIMMER_STATUS, (cursor.getString(cursor.getColumnIndex(DIMMER_STATUS))));
                switchs.put(DIMMER_VALUE, (cursor.getString(cursor.getColumnIndex(DIMMER_VALUE))));
                switchs.put(ROOM_NAME,(cursor.getString(cursor.getColumnIndex(ROOM_NAME))));
                switchs.put(HIDE,(cursor.getString(cursor.getColumnIndex(HIDE))));
                switchs.put(SWITCH_IMAGE_ID,(cursor.getString(cursor.getColumnIndex(SWITCH_IMAGE_ID))));
                // switchs.put(TIME,(cursor.getString(cursor.getColumnIndex(TIME))));

                // Adding contact to list
                listSwitch.add(switchs);
            } while (cursor.moveToNext());
        }

        return listSwitch;
    }
    public void insertNotificationMessage(String activityid, String message, String formattedDate, String mNotiDateTime,String roomName,String image) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NOTIFICATION_MSG,message);
        values.put(NOTIFICATION_READ_FLAG,"0");
        values.put(NOTI_DATE_TIME,formattedDate);
        values.put(ACTIVITY_TIME,mNotiDateTime);
        values.put(ACTIVITY_ID,activityid);
        values.put(ROOM_NAME,roomName);
        values.put(USER_IMAGE,image);

        String mQuery="DELETE FROM "+TABLE_NOTIFICATION+" WHERE "+ACTIVITY_ID+" NOT IN (SELECT "+ACTIVITY_ID+" FROM "+TABLE_NOTIFICATION+" ORDER BY datetime("+NOTI_DATE_TIME+") DESC LIMIT 74)";

        Cursor cursor1=db.rawQuery(mQuery,null);

        Log.d("Notification delete",""+cursor1.getCount());

        Cursor cursor = db.rawQuery("select * from "+TABLE_NOTIFICATION+" where "+ACTIVITY_ID+"='"+activityid+"'",null);

        try {
            if(cursor!=null && cursor.getCount()>0){
                //long mStatus=db.update(TABLE_NOTIFICATION, values, ROOM_ID + "=" + room.get(ROOM_ID),null);
                Log.d("Activity Updated","Id"+activityid);
            }else
            {
                long mStatus=db.insert(TABLE_NOTIFICATION, null, values);
                Log.d("Activity Insert ","Id "+values);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            cursor.close();
            db.close();
        }
    }
    public ArrayList<LinkedHashMap<String,String>> getNotificationMessage() {

        ArrayList<LinkedHashMap<String,String>> msgList=new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATION +" ORDER BY datetime("+NOTI_DATE_TIME+") DESC ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list

        if (cursor.moveToNext()) {
            do {
                LinkedHashMap<String, String> roomType = new LinkedHashMap<>();
                roomType.put(NOTIFICATION_MSG,cursor.getString(cursor.getColumnIndex(NOTIFICATION_MSG)));
                roomType.put(NOTIFICATION_READ_FLAG,cursor.getString(cursor.getColumnIndex(NOTIFICATION_READ_FLAG)));
                roomType.put(USER_IMAGE,cursor.getString(cursor.getColumnIndex(USER_IMAGE)));
                roomType.put(ROOM_NAME,cursor.getString(cursor.getColumnIndex(ROOM_NAME)));
                roomType.put(ACTIVITY_TIME,cursor.getString(cursor.getColumnIndex(ACTIVITY_TIME)));
                // Adding contact to list
                msgList.add(roomType);
            } while (cursor.moveToNext());
        }
        Log.d("List ",""+msgList);
        cursor.close();
        db.close();
        return msgList;
    }

    public int getNotificationCount() {
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATION ;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
        return count;

    }
    public void deleteRoom(String roomId) {

        ContentValues values = new ContentValues();
        values.put(ROOM_ID, roomId);
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_ROOMS, ROOM_ID + "=" + roomId, null);
    }
    public void deleteSwitch(String switchId) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SWITCHES, SWITCH_ID + "=" + switchId, null);
    }
    public void deleteRoomData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ROOMS,null,null);
        db.close();
    }
    public void deleteSwitchData()  {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SWITCHES,null,null);
        db.close();
    }
    public  void deleteRecentData()  {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RECENT,null,null);

        db.close();
    }
    public void deleteNotificationData()  {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NOTIFICATION,null,null);
        db.close();
    }
    public void deleteROOM_TYPE()  {
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(TABLE_ROOM_TYPE,null,null);
        db.close();
    }
    public void deleteSWITCH_TYPE() {
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(TABLE_SWITCH_TYPE,null,null);
        db.close();
    }
    public void deleteModeType() {
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(MODE_TABLE,null,null);
        db.close();
    }

    public void deleteScheduleList() {
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(TABLE_DATETIME,null,null);
        db.close();
    }

    public void insertMsg(int countmsg) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NOTIFICATION_MSG,countmsg);

        try {
            // Inserting Row
            db.insert(TABLE_NOTIFICATION, null, values);

        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();
    }
    public int getUnReadNotificationMessage() {
        int count=0;
        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATION +" where "+NOTIFICATION_READ_FLAG+"='0'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        count = cursor.getCount();
        cursor.close();
        db.close();
        return count;
    }
    public void updateNotificationFlag() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NOTIFICATION_READ_FLAG,"1");
        long result=db.update(TABLE_NOTIFICATION,values, NOTIFICATION_READ_FLAG+ "='0'",null);
        Log.d("Upadte count",result+"");
        db.close();
    }

    public int getSwitchRoomStatusCount(String roomId) {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SWITCHES + " where " + SWITCH_STATUS + " = '1' AND "+ROOM_ID+"='"+roomId+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
        return count;
    }


    public void updateSwitchImageId(String mSwitchID, String switchImageID) {
        ContentValues values = new ContentValues();
        values.put(SWITCH_IMAGE_ID,switchImageID);
        SQLiteDatabase db = this.getWritableDatabase();
        long l=db.update(TABLE_SWITCHES, values, SWITCH_ID + "=" + mSwitchID, null);
        Log.d("Switch ID Update",""+l);
        db.close();
    }


    public void updateSwitchRoomName(String mSwitchID,String roomName) {
        ContentValues values = new ContentValues();
        values.put(SWITCH_NAME,roomName);
        SQLiteDatabase db = this.getWritableDatabase();
        long l=db.update(TABLE_SWITCHES, values, SWITCH_ID + "='" + mSwitchID+"'", null);
        Log.d("Switch ID Update",""+l);
        db.close();
    }

    public void updateRoomHidestatus(String roomId, String hideStatus) {
        ContentValues values = new ContentValues();
        values.put(HIDE,hideStatus);
        SQLiteDatabase db = this.getWritableDatabase();
        long l=db.update(TABLE_ROOMS, values, ROOM_ID + "="+roomId, null);
        Log.d("Switch ID Update",""+l);
        db.close();
    }

    public ArrayList<HashMap<String,String>> getHideRooms() {
        ArrayList<HashMap<String, String>> listRoom = new ArrayList<HashMap<String, String>>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ROOMS + " where " + HIDE + " = '1'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToNext()) {
            do {
                HashMap<String, String> switchs = new HashMap<String, String>();
                switchs.put(ROOM_ID, (cursor.getString(cursor.getColumnIndex(ROOM_ID))));
                switchs.put(ROOM_NAME, (cursor.getString(cursor.getColumnIndex(ROOM_NAME))));
                switchs.put(ROOM_IMAGE_TYPE, (cursor.getString(cursor.getColumnIndex(ROOM_IMAGE_TYPE))));

                // Adding contact to list
                listRoom.add(switchs);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return listRoom;
    }

    public void deleteRoomSwitchData(String mRoomId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+TABLE_SWITCHES+" where "+ROOM_ID+"='"+mRoomId+"'");
        db.close();
    }

    public HashMap<String,String> getScheduleData(String mSwitchId) {
        HashMap<String,String> mMap=new HashMap<>();
        String selectQuery = "SELECT  * FROM " + TABLE_DATETIME + " where " + SWITCH_ID + " = '"+mSwitchId+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToNext()) {
            do {

                mMap.put(SWITCH_ID, (cursor.getString(cursor.getColumnIndex(SWITCH_ID))));
                mMap.put(SWITCH_NAME, (cursor.getString(cursor.getColumnIndex(SWITCH_NAME))));
                mMap.put(SWITCH_STATUS, (cursor.getString(cursor.getColumnIndex(SWITCH_STATUS))));
                mMap.put(IP, (cursor.getString(cursor.getColumnIndex(IP))));

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return mMap;

    }

    public void updateSwitchByType(String mRoomId, String strSwitchNubmer,String strSwitchStatus) {
        ContentValues values = new ContentValues();
        values.put(SWITCH_STATUS,strSwitchStatus);
        SQLiteDatabase db = this.getWritableDatabase();
        long l=db.update(TABLE_SWITCHES, values, ROOM_ID + "='"+mRoomId+"' AND "+SWITCH_TYPE_ID+"='"+strSwitchNubmer+"'", null);
        Log.d("Switch ID Update",""+l);
        db.close();
    }

    public void updateSwitchRoomNameByRoomId(String roomId, String mRoomName) {

        ContentValues values = new ContentValues();
        values.put(ROOM_NAME,mRoomName);
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_SWITCHES, values, ROOM_ID + "=" + roomId, null);
        db.close();

    }

    public void updateSwitchRoomNameInRecent(String roomId, String mRoomName) {
        ContentValues values = new ContentValues();
        values.put(ROOM_NAME,mRoomName);
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_RECENT, values, ROOM_ID + "=" + roomId, null);
        db.close();
    }

    public void deleteSchedule(String mScheduleId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+TABLE_DATETIME+" where "+SCHEDULE_ID+"='"+mScheduleId+"'");
        db.close();
    }

    public void updateSwitchSchedule(String scheduleSwitchId, String scheduleDateTime, String switchStatus,String time,String dimmerVal) {
        ContentValues values = new ContentValues();
        values.put(SCHEDULE_DATETIME,scheduleDateTime);
        values.put(SWITCH_STATUS,switchStatus);
        values.put(TIME,time);
        values.put(DIMMER_VALUE,dimmerVal);
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_DATETIME, values, SCHEDULE_ID + "=" + scheduleSwitchId, null);
        db.close();
    }

    public ArrayList<HashMap<String,String>> getAllSwitches() {
        ArrayList<HashMap<String,String>> arrList=new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_SWITCHES + " where " + HIDE + " = '0'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToNext()) {
            do {

                HashMap<String,String> mMap=new HashMap<>();
                mMap.put(SWITCH_ID, (cursor.getString(cursor.getColumnIndex(SWITCH_ID))));
                mMap.put(SWITCH_NAME, (cursor.getString(cursor.getColumnIndex(SWITCH_NAME))));
                mMap.put(SWITCH_STATUS, (cursor.getString(cursor.getColumnIndex(SWITCH_STATUS))));
                mMap.put(ROOM_NAME, (cursor.getString(cursor.getColumnIndex(ROOM_NAME))));
                mMap.put(SELECTED_SWITCH, (cursor.getString(cursor.getColumnIndex(SELECTED_SWITCH))));
                mMap.put(SWITCH_IMAGE_ID, (cursor.getString(cursor.getColumnIndex(SWITCH_IMAGE_ID))));

                arrList.add(mMap);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return arrList;

    }

    public ArrayList<String> getSelectedAllSwitches() {

        ArrayList<String> arrList=new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_SWITCHES + " where " + HIDE + " = '0' AND "+SELECTED_SWITCH+"='1'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToNext()) {
            do {

                arrList.add((cursor.getString(cursor.getColumnIndex(SWITCH_ID))));

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return arrList;

    }

    public void updateSelectedSwitchStatus(String switch_id, String mSelectSwitch,String mSwitchStatus) {
        ContentValues values = new ContentValues();
        values.put(SELECTED_SWITCH,mSelectSwitch);
        values.put(SWITCH_STATUS,mSwitchStatus);
        SQLiteDatabase db = this.getWritableDatabase();
        long l=db.update(TABLE_SWITCHES,values, SWITCH_ID + "=" + switch_id, null);
        Log.d("Selected Switch-->",""+values);
        db.close();
    }


    public void updateSwitchStatusMqtt(String switchid, String switchstatus,String dimmerVal) {

        ContentValues values = new ContentValues();
        values.put(SWITCH_STATUS,switchstatus);
        values.put(DIMMER_VALUE,dimmerVal);
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_SWITCHES,values, SWITCH_ID + "=" + switchid, null);

        db.close();
    }

    public void updateSwitchStatusAllMqtt(String switchid, String switchstatus) {
        ContentValues values = new ContentValues();
        values.put(SWITCH_STATUS,switchstatus);
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_SWITCHES,values, SWITCH_ID + "=" + switchid, null);
        db.close();
    }

    public void updateSwitchNameMqtt(String switchid, String switchName,String switchImageId) {
        ContentValues values = new ContentValues();
        values.put(SWITCH_NAME,switchName);
        values.put(SWITCH_IMAGE_ID,switchImageId);
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_SWITCHES,values, SWITCH_ID + "=" + switchid, null);
        db.close();
    }

    public void updateHideMqtt(String switchid, String hideStatus,String switchStatus) {
        ContentValues values = new ContentValues();
        values.put(HIDE,hideStatus);
        values.put(SWITCH_STATUS,switchStatus);
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_SWITCHES,values, SWITCH_ID + "=" + switchid, null);
        db.close();
    }

    public void updateLockMqtt(String switchid, String lockstatus) {
        ContentValues values = new ContentValues();
        values.put(LOCK,lockstatus);
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_SWITCHES,values, SWITCH_ID + "=" + switchid, null);
        db.close();
    }

    public void updateRoomNameMqtt(String roomId, String roomName,String roomImage) {
        ContentValues values = new ContentValues();
        values.put(ROOM_NAME,roomName);
        values.put(ROOM_IMAGE_TYPE,roomImage);
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_ROOMS,values, ROOM_ID + "=" + roomId, null);
        db.close();
    }

    public void changeRoomSwitchStatus(String roomId, String switchstatus) {
        ContentValues values = new ContentValues();
        values.put(SWITCH_STATUS,switchstatus);
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_SWITCHES,values, ROOM_ID + "=" + roomId, null);
        db.close();
    }

    public void updateRecntSwitchImageId(String mSwitchID, String switchImageID,String switchName) {
        ContentValues values = new ContentValues();
        values.put(SWITCH_IMAGE_ID,switchImageID);
        values.put(SWITCH_NAME,switchName);
        SQLiteDatabase db = this.getWritableDatabase();
        long l=db.update(TABLE_RECENT, values, SWITCH_ID + "=" + mSwitchID, null);
        Log.d("InRecent ID Update",""+l);
        db.close();
    }

    public boolean isAlreadyONOFF(String roomId, String status) {

        String statusZFinal=status.equals("0")?"1":"0";

        String selectQuery = "SELECT  * FROM " + TABLE_SWITCHES + " where " + SWITCH_STATUS + " = '"+statusZFinal+"' AND "+ROOM_ID+"='"+roomId+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        boolean countflag;
        Log.d("C0unt",cursor.getCount()+"");

        if(cursor.getCount()==0){
            countflag=false;
        }else{
            countflag=true;
        }

        cursor.close();
        db.close();

        return countflag;
    }

    public boolean isDimmerSwitch(String siwtchid) {
        boolean mDimmerStatus;
        String selectQuery = "SELECT  * FROM " + TABLE_SWITCHES + " where " + SWITCH_ID + " = '"+siwtchid+"' AND "+DIMMER_STATUS+"='1' AND "+DIMMER_VALUE+"='0'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.getCount()==0){
            mDimmerStatus=false;
        }else{
            mDimmerStatus=true;
        }

        Log.d("Dimmer status",mDimmerStatus+"");

        return mDimmerStatus;
    }

    public void updateDimmerSwitch(String roomId, String switchstatus) {
        ContentValues values = new ContentValues();
        values.put(DIMMER_VALUE,"38");
        SQLiteDatabase db = this.getWritableDatabase();
        long l=db.update(TABLE_SWITCHES, values, ROOM_ID + "=" + roomId+" AND "+DIMMER_STATUS+"='1' AND "+DIMMER_VALUE+"='0'", null);
        Log.d("InRecent ID Update",""+l);
        db.close();
    }

    public ArrayList<HashMap<String,String>> getSchedulerSwitchWiseInfo(String switchID) {

        ArrayList<HashMap<String, String>> listSchedule = new ArrayList<HashMap<String, String>>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_DATETIME +" Where "+SWITCH_ID+"='"+switchID+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToNext()) {
            do {
                HashMap<String, String> mMap = new HashMap<String, String>();
                mMap.put(SWITCH_NAME, (cursor.getString(cursor.getColumnIndex(SWITCH_NAME))));
                mMap.put(SWITCH_ID, (cursor.getString(cursor.getColumnIndex(SWITCH_ID))));
                mMap.put(SWITCH_STATUS, (cursor.getString(cursor.getColumnIndex(SWITCH_STATUS))));
                mMap.put(SCHEDULE_DATETIME, (cursor.getString(cursor.getColumnIndex(SCHEDULE_DATETIME))));
                mMap.put(TIME, (cursor.getString(cursor.getColumnIndex(TIME))));
                mMap.put(ROOM_NAME,(cursor.getString(cursor.getColumnIndex(ROOM_NAME))));
                mMap.put(SCHEDULE_ID,(cursor.getString(cursor.getColumnIndex(SCHEDULE_ID))));
                mMap.put(DIMMER_VALUE,(cursor.getString(cursor.getColumnIndex(DIMMER_VALUE))));
                mMap.put(DIMMER_STATUS,(cursor.getString(cursor.getColumnIndex(DIMMER_STATUS))));
                // Adding contact to list
                listSchedule.add(mMap);
            } while (cursor.moveToNext());
        }

        Log.d("Schedule All Data : ",""+listSchedule);
        return listSchedule;
    }

    public void createMode(String name) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(MODE_NAME,name);

        try {
                long mStatus=db.insert(MODE_TABLE, null, values);
                Log.d(TAG, "insert mode  "+mStatus);

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            db.close();
        }
    }

    public ArrayList<HashMap<String,String>> getAllModeInfo() {

        ArrayList<HashMap<String,String>> modeData=new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + MODE_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToNext()) {
            do {
                HashMap<String,String> map=new HashMap<>();
                map.put(MODE_ID,cursor.getString(cursor.getColumnIndex(MODE_ID)));
                map.put(MODE_NAME,cursor.getString(cursor.getColumnIndex(MODE_NAME)));
                map.put(ON_MODE,cursor.getString(cursor.getColumnIndex(ON_MODE)));
                map.put(OFF_MODE,cursor.getString(cursor.getColumnIndex(OFF_MODE)));
                map.put(REPEATE,cursor.getString(cursor.getColumnIndex(REPEATE)));
                map.put(ISMODE_SELECTED,cursor.getString(cursor.getColumnIndex(ISMODE_SELECTED)));
                map.put(ISSHORCUT,cursor.getString(cursor.getColumnIndex(ISSHORCUT)));
                modeData.add(map);

            } while (cursor.moveToNext());
        }

        db.close();

        return modeData;
    }

    public void updateModeStatus(String strSwitchONID, String strSwitchOFFID,String Id) {
        ContentValues values = new ContentValues();
        values.put(ON_MODE,strSwitchONID);
        values.put(OFF_MODE,strSwitchOFFID);
        SQLiteDatabase db = this.getWritableDatabase();
        long l=db.update(MODE_TABLE, values, MODE_ID + "=" + Id, null);
        Log.d("Mode Table on/off",""+l);
        db.close();
    }

    public HashMap<String,String> getModeInfoByID(String modeid) {

        HashMap<String,String> modeData=new HashMap<>();
        String selectQuery = "SELECT  * FROM " + MODE_TABLE +" where "+MODE_ID+"="+modeid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        try {

            if (cursor.moveToNext()) {
                do {
                    modeData.put(MODE_ID, cursor.getString(cursor.getColumnIndex(MODE_ID)));
                    modeData.put(MODE_NAME, cursor.getString(cursor.getColumnIndex(MODE_NAME)));
                    modeData.put(ON_MODE, cursor.getString(cursor.getColumnIndex(ON_MODE)));
                    modeData.put(OFF_MODE, cursor.getString(cursor.getColumnIndex(OFF_MODE)));
                    modeData.put(REPEATE, cursor.getString(cursor.getColumnIndex(REPEATE)));
                    modeData.put(ISMODE_SELECTED, cursor.getString(cursor.getColumnIndex(ISMODE_SELECTED)));
                    modeData.put(ISSHORCUT,cursor.getString(cursor.getColumnIndex(ISSHORCUT)));

                } while (cursor.moveToNext());
            }
        }catch(Exception e){}
        finally {
            cursor.close();
            db.close();

        }

        return modeData;
    }

    public void deleteMode(String modeId) {
        SQLiteDatabase db=this.getWritableDatabase();
        db.execSQL("DELETE FROM "+MODE_TABLE+" WHERE "+MODE_ID+"="+modeId+"");
        db.close();
    }

    public void updateActiveFlag(String modeId) {
        ContentValues values = new ContentValues();
        values.put(ISMODE_SELECTED,"1");
        SQLiteDatabase db = this.getWritableDatabase();
        long l=db.update(MODE_TABLE, values, MODE_ID + "=" + modeId, null);
        db.close();
    }

    public void updateAllActiveFlag() {
        ContentValues values = new ContentValues();
        values.put(ISMODE_SELECTED,"0");
        SQLiteDatabase db = this.getWritableDatabase();
        long l=db.update(MODE_TABLE, values, null, null);
        db.close();
    }

    public void updateModeName(String modeId,String modeName) {
        ContentValues values = new ContentValues();
        values.put(MODE_NAME,modeName);
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            long l = db.update(MODE_TABLE, values, MODE_ID + "=" + modeId, null);
        }catch(Exception e){}
        db.close();
    }

    public String updateModeShorcutFlag(HashMap<String,String> mMap,String shorcut){

        ContentValues values = new ContentValues();
        values.put(ISSHORCUT,shorcut);
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("select * from "+MODE_TABLE+" where "+ISSHORCUT+"='1'",null);

        try {
            if(cursor.getCount()<3) {
                long l = db.update(MODE_TABLE, values, MODE_ID + "=" + mMap.get(MODE_ID), null);
                return shorcut.equals("1")?"Shortcut added successfully":"Shortcut removed successfully";
            }else{
                if(shorcut.equals("0")){
                    long l = db.update(MODE_TABLE, values, MODE_ID + "=" + mMap.get(MODE_ID), null);
                    return shorcut.equals("1")?"Shortcut added successfully":"Shortcut removed successfully";
                }
                return "Maximum three shortcuts are allowed";
            }
        }catch(Exception e){}
        finally {
            cursor.close();
            db.close();
        }

       return "";
    }

    public ArrayList<HashMap<String,String>> getShorcutFromMode() {

        ArrayList<HashMap<String,String>> modeData=new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + MODE_TABLE +" where "+ISSHORCUT+"='1'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        try {
            if (cursor.moveToNext()) {
                do {
                    HashMap<String, String> map = new HashMap<>();
                    map.put(MODE_ID, cursor.getString(cursor.getColumnIndex(MODE_ID)));
                    map.put(MODE_NAME, cursor.getString(cursor.getColumnIndex(MODE_NAME)));
                    map.put(ON_MODE, cursor.getString(cursor.getColumnIndex(ON_MODE)));
                    map.put(OFF_MODE, cursor.getString(cursor.getColumnIndex(OFF_MODE)));
                    map.put(REPEATE, cursor.getString(cursor.getColumnIndex(REPEATE)));
                    map.put(ISMODE_SELECTED, cursor.getString(cursor.getColumnIndex(ISMODE_SELECTED)));
                    map.put(ISSHORCUT, cursor.getString(cursor.getColumnIndex(ISSHORCUT)));
                    modeData.add(map);

                } while (cursor.moveToNext());
            }
        }catch(Exception e){}
        finally {

            cursor.close();
            db.close();
        }

        return modeData;
    }

    public HashMap<String,String> getSwitchInfoBySwitchId(String SwitchId) {

        String mSwitchType=null;
        String selectQuery = "SELECT  * FROM " + TABLE_SWITCHES +" where "+SWITCH_ID+"='"+SwitchId+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        HashMap<String,String> switchs=new HashMap<>();
        if (cursor.moveToNext()) {
            do {
                switchs.put(SWITCH_ID, "" + Integer.parseInt(cursor.getString(cursor.getColumnIndex(SWITCH_ID))));
                switchs.put(SWITCH_NAME, (cursor.getString(cursor.getColumnIndex(SWITCH_NAME))));
                switchs.put(SWITCH_TYPE_ID, (cursor.getString(cursor.getColumnIndex(SWITCH_TYPE_ID))));
                switchs.put(SWITCH_STATUS, (cursor.getString(cursor.getColumnIndex(SWITCH_STATUS))));
                switchs.put(ROOM_ID, (cursor.getString(cursor.getColumnIndex(ROOM_ID))));
                switchs.put(DIMMER_STATUS, (cursor.getString(cursor.getColumnIndex(DIMMER_STATUS))));
                switchs.put(DIMMER_VALUE, (cursor.getString(cursor.getColumnIndex(DIMMER_VALUE))));
                switchs.put(LOCK,cursor.getString(cursor.getColumnIndex(LOCK)));
                switchs.put(HIDE,cursor.getString(cursor.getColumnIndex(HIDE)));
                switchs.put(ROOM_NAME,cursor.getString(cursor.getColumnIndex(ROOM_NAME)));
                switchs.put(SWITCH_IMAGE_ID,cursor.getString(cursor.getColumnIndex(SWITCH_IMAGE_ID)));

            } while (cursor.moveToNext());
        }

        return switchs;
    }


}
