package Fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.genieiot.gsmarthome.ActivityService;
import com.genieiot.gsmarthome.LivingRoom;
import com.genieiot.gsmarthome.R;
import com.genieiot.gsmarthome.SwitchSelectionActivity;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.readystatesoftware.viewbadger.BadgeView;

import net.hockeyapp.android.UpdateManager;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import Database.DatabaseHandler;
import Session.Constants;
import Session.SessionManager;

import static Database.DatabaseHandler.MODE_ID;
import static Database.DatabaseHandler.MODE_NAME;
import static Database.DatabaseHandler.OFF_MODE;
import static Database.DatabaseHandler.ON_MODE;
import static Session.Constants.URL_GENIE;


/**
 * Created by Genie IoT on 9/13/2016.
 */
public class Home extends Fragment implements View.OnClickListener{

    private View view;
    private ViewPager viewPager;
    private android.support.design.widget.TabLayout tabLayout;
    private TextView txtCount,tab_badge,tvInternetMsg;
    int count1;
    FloatingActionMenu fab;
    FloatingActionButton turnOffHome,turnOnHome,shortcut1;
    SessionManager session;
    private static final String TAG = "Home";
    private ProgressDialog pDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.home, container, false);

        fab= (FloatingActionMenu) view.findViewById(R.id.fabadd);
        fab.setOnClickListener(this);
        fab.setClosedOnTouchOutside(true);
        fab.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean opened) {
                if(opened){
                    DatabaseHandler db=new DatabaseHandler(getActivity());
                    ArrayList<HashMap<String,String>> arrShortcut=db.getShorcutFromMode();
                    if(arrShortcut.size()==0){
                        Toast.makeText(getActivity(),"Add new mode using Profile", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });



        turnOffHome= (FloatingActionButton) view.findViewById(R.id.turnOffHome);
        turnOnHome= (FloatingActionButton) view.findViewById(R.id.turnOnHome);
        shortcut1= (FloatingActionButton) view.findViewById(R.id.shortcut1);

        turnOffHome.setOnClickListener( this);
        turnOnHome.setOnClickListener(this);
        shortcut1.setOnClickListener(this);

        DatabaseHandler db=new DatabaseHandler(getActivity());
        setShortcutMenu();

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        txtCount= (TextView) view.findViewById(R.id.txtCount);
        tvInternetMsg= (TextView) view.findViewById(R.id.tvInternetMsg);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        session=new SessionManager(getActivity());

        createTabIcons();

        String strIP=getIP();

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {
            Log.d("Device", "onCreateView: tablet");
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        } else {
            Log.d("Device", "onCreateView: mobile");
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }

        count1=db.getNotificationCount();
        viewPager.setCurrentItem(0);


        Log.d("home_call","call api");


        return view;
    }

    private String getIP() {
         try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("IP Address", ex.toString());
        }
        return null;

    }
    private void createTabIcons() {

        LinearLayout tabView=(LinearLayout)LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab,null);
        TextView tv= (TextView) tabView.findViewById(R.id.tab);
        tv.setText("ROOMS");
        tabLayout.getTabAt(0).setCustomView(tabView);

        LinearLayout tabView1=(LinearLayout)LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab,null);
        TextView tv1= (TextView) tabView1.findViewById(R.id.tab);
        tv1.setText("RECENTS");
        tabLayout.getTabAt(3).setCustomView(tabView1);

        LinearLayout tabView2=(LinearLayout)LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab,null);
        TextView tv2= (TextView) tabView2.findViewById(R.id.tab);
        tab_badge=(TextView)tabView2.findViewById(R.id.tab_badge);
        tv2.setText("ACTIVITY");
        tabLayout.getTabAt(2).setCustomView(tabView2);

        LinearLayout tabView3=(LinearLayout)LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab,null);
        TextView tv3= (TextView) tabView3.findViewById(R.id.tab);
        tv3.setText("PROFILE");
        tabLayout.getTabAt(1).setCustomView(tabView3);

        setActivityCount();

    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFragment(new Rooms(), "ROOMS");
        adapter.addFragment(new ProfileFragment(),"PROFILE");
        adapter.addFragment(new Activities(),"ACTIVITY");
        adapter.addFragment(new Recent(), "RECENTS");
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if(position==2){

                    DatabaseHandler database=new DatabaseHandler(getActivity());
                    database.updateNotificationFlag();
                    setActivityCount();
                    getActivity().startService(new Intent(getActivity(),ActivityService.class));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void onClick(View v)  {
        switch (v.getId()) {
            case R.id.turnOffHome:
                Log.d(TAG, "onClick: "+turnOffHome.getTag());
                fab.close(true);
                HashMap<String,String> map= (HashMap<String, String>) turnOffHome.getTag();
                callModeActivationService(map);
                break;
            case R.id.turnOnHome:
                //onIntentSelection("1");
                fab.close(true);
                HashMap<String,String> map1= (HashMap<String, String>) turnOnHome.getTag();
                callModeActivationService(map1);
                break;
            case R.id.shortcut1:
                Log.d(TAG, "onClick: "+shortcut1.getTag());
                fab.close(true);
                HashMap<String,String> map2= (HashMap<String, String>) shortcut1.getTag();
                callModeActivationService(map2);
                break;

            case R.id.fabadd:
                DatabaseHandler db=new DatabaseHandler(getActivity());
                ArrayList<HashMap<String,String>> arrShortcut=db.getShorcutFromMode();
                if(arrShortcut.size()==0){
                    Toast.makeText(getActivity(),"Add new mode using Profile", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    private void callModeActivationService(HashMap<String,String> map){
        if((map.get(ON_MODE)==null || map.get(ON_MODE).isEmpty()) && (map.get(OFF_MODE)==null || map.get(OFF_MODE).isEmpty())){

            Intent intentON = new Intent(getActivity(), SwitchSelectionActivity.class);
            intentON.putExtra("ACTION","00");
            intentON.putExtra("ID",map.get(MODE_ID));
            startActivityForResult(intentON, 2);

        }else{
            new AsyncModeActivationTask().execute(map.get(MODE_ID));
        }
    }


    private void onIntentSelection(String action){
        Intent intentON = new Intent(getActivity(), SwitchSelectionActivity.class);
        intentON.putExtra("ACTION",action);
        startActivity(intentON);
        fab.close(true);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public View addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position)
            {
                case 0:return mFragmentTitleList.get(position);
                case 1:return mFragmentTitleList.get(position);
                case 2:return mFragmentTitleList.get(position);
                case 3:return mFragmentTitleList.get(position)+"("+count1+")";
            }

            return mFragmentTitleList.get(position);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        Log.d("Home Fragment-->","onResume()");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(message, new IntentFilter("NotificationSend"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(message, new IntentFilter("refreshShortcut"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(message, new IntentFilter("INTERNET_TEST"));

        setTotalCount();
        
        setImageBubble();
        UpdateManager.register(getActivity());
    }

    private void setImageBubble() {
    }

    private void setTotalCount() {
        int count=0;
        DatabaseHandler db1 = new DatabaseHandler(getActivity());
        count = db1.getSwitchStatusCount();
        txtCount.setText(String.valueOf(count));

    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(message);
    }

    private BroadcastReceiver message = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            if(intent.hasExtra("KEY")){
                setShortcutMenu();
            }if(intent.hasExtra("INTERNET_TEST")){
                setInternetSataus(intent.getStringExtra("INTERNET_TEST"));
            }else {
                setActivityCount();
                setTotalCountNew();
            }
        }
    };

    private void setInternetSataus(String internet_test) {
        if(getActivity()!=null){
            if(internet_test.equals("0")){
                tvInternetMsg.setVisibility(View.VISIBLE);
            }  else{
                tvInternetMsg.setVisibility(View.GONE);
            }
        }

    }

    private void setShortcutMenu() {

        turnOnHome.setVisibility(View.GONE);
        turnOffHome.setVisibility(View.GONE);
        shortcut1.setVisibility(View.GONE);

        DatabaseHandler db=new DatabaseHandler(getActivity());
        ArrayList<HashMap<String,String>> arrShortcut=db.getShorcutFromMode();
        for(int i=0;i<arrShortcut.size();i++){
            if(i==0){
                String a =arrShortcut.get(i).get(MODE_NAME);

                turnOnHome.setVisibility(View.VISIBLE);
                turnOnHome.setLabelText(arrShortcut.get(i).get(MODE_NAME));
                turnOnHome.setTag(arrShortcut.get(i));
                //turnOnHome.setTag(1,arrShortcut.get(i));
            }else if(i==1){
                String a =arrShortcut.get(i).get(MODE_NAME);
                turnOffHome.setVisibility(View.VISIBLE);
                turnOffHome.setLabelText(arrShortcut.get(i).get(MODE_NAME));
                turnOffHome.setTag(arrShortcut.get(i));
            }else if(i==2){
                String a =arrShortcut.get(i).get(MODE_NAME);
                shortcut1.setVisibility(View.VISIBLE);
                shortcut1.setLabelText(arrShortcut.get(i).get(MODE_NAME));
                shortcut1.setTag(arrShortcut.get(i));
            }
        }
    }

    private void setTotalCountNew() {
        int count=0;
        DatabaseHandler db1 = new DatabaseHandler(getActivity());
        count = db1.getSwitchStatusCount();
        txtCount.setText(String.valueOf(count));
    }

    public void setActivityCount(){

        if(getActivity()!=null) {
            DatabaseHandler database = new DatabaseHandler(getActivity());
            int countmsg = database.getUnReadNotificationMessage();

            if (countmsg > 0) {
                tab_badge.setVisibility(View.VISIBLE);
                tab_badge.setText(countmsg + "");
            } else {
                tab_badge.setVisibility(View.GONE);
            }
        }
    }

    private class AsyncModeActivationTask extends AsyncTask<String,Void,String> {
        DatabaseHandler db;
        HashMap<String,String> modeData;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Completing your wish...");
            pDialog.setCancelable(false);
            pDialog.show();
            db=new DatabaseHandler(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            Constants req=new Constants();
            String response=null;
            SessionManager sessionManager=new SessionManager(getActivity());

            modeData=db.getModeInfoByID(params[0]);
            try {
                JSONObject jObj=new JSONObject();
                jObj.put("userId",sessionManager.getUSERID());
                String mOFFData=modeData.get(OFF_MODE);
                String mONData=modeData.get(ON_MODE);

//                if(mONData.isEmpty()){
//                    jObj.put("onoffstatus","0");
//                    jObj.put("switchId",modeData.get(OFF_MODE));
//                    jObj.put("profileSwitchId","NA");
//                    jObj.put("profileOnOffStatus","NA");
//                }else {
                jObj.put("onoffstatus", "1");
                jObj.put("switchId",mONData);
                jObj.put("profileSwitchId", mOFFData);
                jObj.put("profileOnOffStatus","0");
//                }
                jObj.put("messageFrom", "Local");
                jObj.put("modeName",modeData.get(MODE_NAME));
                Log.d("All Switches-->",jObj+"");

                response=req.doPostRequest(URL_GENIE + "/switch/turnOnOffAll", jObj+"", sessionManager.getSecurityToken());

            }catch (Exception e){}

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(pDialog!=null && pDialog.isShowing()){
                pDialog.dismiss();
            }

            if(result!=null){
                Log.d(TAG, "onPostExecute: "+result);
                try {
                    JSONObject jResult = new JSONObject(result);
                    if (jResult.getString("status").equals("SUCCESS")) {
                        JSONArray jMain=new JSONArray(jResult.getString("result"));

                        for(int i=0;i<jMain.length();i++) {
                            JSONObject jArr=jMain.getJSONObject(i);
                            if(!jArr.getString("status").isEmpty()) {
                                db.updateAllSwitchStatus(jArr.getString("siwtchid"), jArr.getString("status"));
                            }else{
                                db.updateAllSwitchStatus(jArr.getString("profileSwitchid"), jArr.getString("profileStatus"));
                            }
                        }

                        Toast.makeText(getActivity(),"Your wish is completed.",Toast.LENGTH_SHORT).show();
                        db.updateAllActiveFlag();
                        db.updateActiveFlag(modeData.get(MODE_ID));
                        //setAdapter();
                    }else{
                        Toast.makeText(getActivity(),getResources().getString(R.string.MSG_TRY_AGAIN),Toast.LENGTH_SHORT).show();
                    }
                }catch(Exception e){}

            }else{
                Toast.makeText(getActivity(),getResources().getString(R.string.MSG_TRY_AGAIN),Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is
        if(requestCode==2)
        {
            setShortcutMenu();
        }
    }
}
