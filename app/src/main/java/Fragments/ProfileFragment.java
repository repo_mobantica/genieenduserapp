package Fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.genieiot.gsmarthome.AddNewTask;
import com.genieiot.gsmarthome.R;
import com.genieiot.gsmarthome.Schedular_Activity;
import com.genieiot.gsmarthome.SwitchSelectionActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import Adapter.ProfileAdapter;
import Database.DatabaseHandler;
import Session.Constants;
import Session.IOnClickListner;
import Session.IOnLongClickListener;
import Session.SessionManager;

import static Database.DatabaseHandler.ISMODE_SELECTED;
import static Database.DatabaseHandler.ISSHORCUT;
import static Database.DatabaseHandler.MODE_ID;
import static Database.DatabaseHandler.MODE_NAME;
import static Database.DatabaseHandler.OFF_MODE;
import static Database.DatabaseHandler.ON_MODE;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;

/**
 * Created by root on 17/3/17.
 */

public class ProfileFragment extends Fragment implements IOnClickListner, IOnLongClickListener {
    private View rootView;
    private RecyclerView recycler_profile;
    private ProfileAdapter adapter;
    private DatabaseHandler db;
    private ProgressDialog pDialog;
    private static final String TAG = "ProfileFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.profile_fragment_layout, container, false);
        recycler_profile=(RecyclerView)rootView.findViewById(R.id.recycler_profile);

        db=new DatabaseHandler(getActivity());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_profile.setLayoutManager(mLayoutManager);

        setAdapter();

        return rootView;
    }

    private void setAdapter() {

        ArrayList<HashMap<String,String>> arrListProfile=new ArrayList<>();
        arrListProfile=db.getAllModeInfo();
        HashMap<String,String> map1=new HashMap<>();
        map1.put(MODE_NAME,"Add New Mode");
        map1.put(ISMODE_SELECTED,"0");
        arrListProfile.add(map1);
        adapter=new ProfileAdapter(getActivity(),arrListProfile,ProfileFragment.this);
        recycler_profile.setAdapter(adapter);

    }

    @Override
    public void onClickListner(HashMap<String, String> map) {

        if(map.get(MODE_NAME).equals("Add New Mode")) {
            showFolderNameDialog("NEW","");
        }else{
            if((map.get(ON_MODE)==null || map.get(ON_MODE).isEmpty()) && (map.get(OFF_MODE)==null || map.get(OFF_MODE).isEmpty())){

                Intent intentON = new Intent(getActivity(), SwitchSelectionActivity.class);
                intentON.putExtra("ACTION","00");
                intentON.putExtra("ID",map.get(MODE_ID));
                startActivityForResult(intentON, 2);

            }else{
                new AsyncModeActivationTask().execute(map.get(MODE_ID));
            }
        }
    }


    private void showFolderNameDialog(final String opt, final String id) {
        final Dialog dialogDeviceID = new Dialog(getActivity());
        // Include dialog.xml file
        dialogDeviceID.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDeviceID.setContentView(R.layout.dailog_name);
        TextView txtOk = (TextView) dialogDeviceID.findViewById(R.id.txtOk);
        TextView tvTitle = (TextView) dialogDeviceID.findViewById(R.id.tvTitle);
        final EditText etName=(EditText)dialogDeviceID.findViewById(R.id.etName);
        final TextView txtCancel = (TextView) dialogDeviceID.findViewById(R.id.txtCancel);

        tvTitle.setText("Mode Name ");
        if(opt.equals("NEW")) {
            txtOk.setText("CREATE");
        }else{
            etName.setText(opt);
            txtOk.setText("RENAME");
        }

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDeviceID.dismiss();
                String name=etName.getText().toString();
                if(!name.equals("")) {
                    dialogDeviceID.dismiss();
                    if(opt.equals("NEW")) {
                        db.createMode(name);
                    }else{
                        db.updateModeName(id,name);
                    }
                    setAdapter();
                }else{
                    Toast.makeText(getActivity(), "Please enter name", Toast.LENGTH_SHORT).show();
                }
            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDeviceID.dismiss();
            }
        });
        dialogDeviceID.show();

    }

    @Override
    public void OnLongClickListener(final HashMap<String, String> mMap) {
        String shorcut;

        if(mMap.get(ISSHORCUT).equals("0")) {
            shorcut= "Add to Shortcut";
        }else{
            shorcut="Remove to Shortcut";
        }

        final String finalShorcut=shorcut;
        final CharSequence[] strArray = {"Edit","Delete","Rename",shorcut};
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setItems(strArray,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                 editSelectedSwitchList(mMap);
                                break;
                            case 1:
                                alertDialogDelete(mMap);
                                break;
                            case 2:
                                renameModeName(mMap);
                                break;
                            case 3:
                                addOrRemoveShorcut(mMap,finalShorcut);
                                break;

                        }
                    }
                });

        alertDialogBuilder.show();

    }

    private void addOrRemoveShorcut(HashMap<String, String> mMap, String shorcut) {
        String message=db.updateModeShorcutFlag(mMap,shorcut.equals("Add to Shortcut")?"1":"0");
        Toast.makeText(getActivity(),message, Toast.LENGTH_LONG).show();
        setAdapter();
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("refreshShortcut").putExtra("KEY","1"));
    }

    private void editSelectedSwitchList(HashMap<String, String> mMap) {
        Intent intentON = new Intent(getActivity(), SwitchSelectionActivity.class);
        intentON.putExtra("ACTION","00");
        intentON.putExtra("ID",mMap.get(MODE_ID));
        intentON.putExtra("EDIT",mMap.get(ON_MODE));
        startActivityForResult(intentON, 2);
    }

    private void renameModeName(HashMap<String, String> mMap) {
        showFolderNameDialog(mMap.get(MODE_NAME),mMap.get(MODE_ID));
    }

    private void alertDialogDelete(final HashMap<String,String> mMap)  {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_delete);

        TextView txtCancel= (TextView) dialog.findViewById(R.id.txtCancel);
        TextView tvTitle= (TextView) dialog.findViewById(R.id.tvTitle);
        TextView tvMessage= (TextView) dialog.findViewById(R.id.tvMessage);
        TextView txtDelete= (TextView) dialog.findViewById(R.id.txtDelete);
        tvTitle.setText("Delete Mode ");
        //tvMessage.setText("Delete Mode ");

        txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //new AddNewTask.AsyncDeleteScheduleTask().execute(mMap);
                //Toast.makeText(getActivity(),mMap.get(MODE_ID),Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                db.deleteMode(mMap.get(MODE_ID));
                setAdapter();
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("refreshShortcut").putExtra("KEY","1"));
                Toast.makeText(getActivity(),mMap.get(MODE_NAME)+" is deleted successfully",Toast.LENGTH_LONG).show();

            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private class AsyncModeActivationTask extends AsyncTask<String,Void,String> {
        HashMap<String,String> modeData;
        String messageType="";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Completing your wish...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Constants req=new Constants();
            String response=null;
            SessionManager sessionManager=new SessionManager(getActivity());

            modeData=db.getModeInfoByID(params[0]);
            try {
                JSONObject jObj=new JSONObject();
                if(URL_GENIE.equals(URL_GENIE_AWS)){
                    messageType=INTERNET;
                }
                else{
                    messageType=LOCAL_HUB;
                }

                jObj.put("userId",sessionManager.getUSERID());
                String mOFFData=modeData.get(OFF_MODE);
                String mONData=modeData.get(ON_MODE);

//                if(mONData.isEmpty()){
//                    jObj.put("onoffstatus","0");
//                    jObj.put("switchId",modeData.get(OFF_MODE));
//                    jObj.put("profileSwitchId","NA");
//                    jObj.put("profileOnOffStatus","NA");
//                }else {
                    jObj.put("onoffstatus", "1");
                    jObj.put("switchId",mONData);
                    jObj.put("profileSwitchId", mOFFData);
                    jObj.put("profileOnOffStatus","0");
//                }
                jObj.put("messageFrom", messageType);
                jObj.put("modeName",modeData.get(MODE_NAME));
                Log.d("All Switches-->",jObj+messageType);

                response=req.doPostRequest(URL_GENIE + "/switch/turnOnOffAll", jObj+"", sessionManager.getSecurityToken());

            }catch (Exception e){}

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(pDialog!=null && pDialog.isShowing()){
                pDialog.dismiss();
            }

            if(result!=null){
                Log.d(TAG, "onPostExecute: "+result);


                if(messageType.equals(INTERNET)){
                    Toast.makeText(getActivity(),"Your wish is completed.",Toast.LENGTH_SHORT).show();
                    db.updateAllActiveFlag();
                    db.updateActiveFlag(modeData.get(MODE_ID));
                    setAdapter();
                }

                try {
                    JSONObject jResult = new JSONObject(result);
                    if (jResult.getString("status").equals("SUCCESS")) {
                        JSONArray jMain=new JSONArray(jResult.getString("result"));

                        for(int i=0;i<jMain.length();i++) {
                            JSONObject jArr=jMain.getJSONObject(i);
                            if(!jArr.getString("status").isEmpty()) {
                                db.updateAllSwitchStatus(jArr.getString("siwtchid"), jArr.getString("status"));
                            }else{
                                db.updateAllSwitchStatus(jArr.getString("profileSwitchid"), jArr.getString("profileStatus"));
                            }
                        }

                        Toast.makeText(getActivity(),"Your wish is completed.",Toast.LENGTH_SHORT).show();
                        db.updateAllActiveFlag();
                        db.updateActiveFlag(modeData.get(MODE_ID));
                        setAdapter();
                    }else{
                        Toast.makeText(getActivity(),getResources().getString(R.string.MSG_TRY_AGAIN),Toast.LENGTH_SHORT).show();
                    }
                }catch(Exception e){
                    Toast.makeText(getContext(), "error"+e.getMessage(), Toast.LENGTH_SHORT).show();
                }

              Log.d("Local",result);
            }else{
                Toast.makeText(getActivity(),getResources().getString(R.string.MSG_TRY_AGAIN),Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is
        if(requestCode==2)
        {
            setAdapter();
        }
    }
}
