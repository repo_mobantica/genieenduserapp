package Fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompatSideChannelService;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.genieiot.gsmarthome.AddRooms;
import com.genieiot.gsmarthome.GetSwitchByRoomService;
import com.genieiot.gsmarthome.LivingRoom;
import com.genieiot.gsmarthome.SwitchSelectionActivity;
import com.genieiot.gsmarthome.ChangeIPActivity;
import com.genieiot.gsmarthome.HideList;
import com.genieiot.gsmarthome.R;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Adapter.AdapterCustomRooms;
import Database.DatabaseHandler;
import Session.Constants;
import Session.SessionManager;
import app.AppController;

import static Database.DatabaseHandler.DIMMER_STATUS;
import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.HIDE;
import static Database.DatabaseHandler.LOCK;
import static Database.DatabaseHandler.OPERATION;
import static Database.DatabaseHandler.ROOM_ID;
import static Database.DatabaseHandler.ROOM_IMAGE_TYPE;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.ROOM_TYPE_ID;
import static Database.DatabaseHandler.SELECTED_SWITCH;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_IMAGE_ID;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.SWITCH_TYPE_ID;

import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.MESSAGE_INTERNET_CONNECTION;
import static Session.Constants.MESSAGE_TRY_AGAIN;
import static Session.Constants.MY_PREFS_NAME;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;
import static Session.SessionManager.X_AUTH_TOKEN;
import static android.content.Context.MODE_PRIVATE;
import static com.genieiot.gsmarthome.MainActivity.isInternetAvailable;
import static com.genieiot.gsmarthome.MainActivity.myToast;

/**
 * Created by Genie IoT on 9/13/2016.
 */
public class Rooms extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private View view;
    Context context;
    public static ArrayList<HashMap<String, String>> roomDatas;
    private ArrayList<String> categories;
    android.support.design.widget.FloatingActionButton mFabAdd;
    private EditText edtSwitchName;
    private String mSwitchName = "";
    private String mSwitchType = "";
    private DatabaseHandler db;
    private int count;
    private TextView txtCount,tvMessageInt;
    private SessionManager sessionManager;
    private String strWebserviceName = "";
    private ProgressDialog pDialog;
    private int methodType;
    private String roomId;
    RecyclerView lstRoom;
    AdapterCustomRooms adapter;
    private String messageType;
    LinearLayout linearLayout;
    SharedPreferences prefs;
    String flagStatus;
    public static boolean Flag=true;
    String broker = "";
    String clientId = "";
    MqttConnectOptions connOpts;
    MqttClient mqqtClient = null;
    MemoryPersistence persistence;
    String topic ;

    JSONObject jMain=new JSONObject();

    String flag;
    SharedPreferences.Editor editor;
    SharedPreferences pref;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.rooms, container, false);
        db = new DatabaseHandler(getActivity());
        setHasOptionsMenu(true);
        roomDatas=new ArrayList<>();
        initWidgets();
         editor = getContext().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
         pref = getContext().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);


//        if (restoredText != null) {
//            String flag = prefs.getString("key", " ");
//            Log.d("Flag_value","=="+flag);
//
//        }

//        if (sessionManager.getUSERID().equals("")) {
//            insertDefaultValuesInDatabase();
//        }





        if (sessionManager.getUSERID().equals("")) {
            insertDefaultValuesInDatabase();
            getRoomListLocal();
            setRoomAdapter();

        }
        else
        {
            if (isInternetAvailable(getActivity()))
            {
                try {
                    try {
                        //     String falg_value=sessionManager.getCountVal();

                        String flag1 = pref.getString("room_key", null);
                        Log.d("Flag_value","=="+flag1);
                        if(flag1.equals("true"))
                        {
                            getRoomListServer();
                            Log.d("room_list","call api");
                            setRoomAdapter();
                            editor.putString("room_key", "false");
                            editor.apply();


                            Log.d("count_value","call api");
                            //  sessionManager.setCountVal("false");
                        }
                        else
                        {
                            Log.d("count_value","call api=1");
                            Log.d("Flag_value","call api=1"+flag1);
                            setRoomAdapter();
                        }




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                setRoomAdapter();
                Log.d("room_list","call api1");
                myToast(getActivity(), MESSAGE_INTERNET_CONNECTION);
            }
        }

        return view;
    }


    private void insertDefaultValuesInDatabase() {

        db = new DatabaseHandler(getActivity());

        if (db.getRoomTypeCount() < 1) {
            insertRoomType();
        }

        if (db.getSwitchTypeCount() < 1) {
            insertSwitchType();
        }

        if (db.getRoomCount() < 1) {
            insertRoom();
        }

        if (db.getSwitchCount() < 1) {
            insertSwitch();
        }
    }
    private void insertSwitch() {
        //this data is for hall
        HashMap<String, String> switcch = new HashMap<String, String>();
        String[] arrHallSwitch={"Chandelier","Socket","Tube","AC","Dimmer Bulb","Fan"};
        String[] arrHallSwitchImage={"2","13","18","1","0","8"};

        for(int i=0;i<6;i++){
            switcch.put(SWITCH_NAME, arrHallSwitch[i]);
            switcch.put(ROOM_NAME, "Hall");
            switcch.put(SWITCH_TYPE_ID,(i+1)+"");
            switcch.put(SWITCH_STATUS, "0");
            switcch.put(ROOM_ID, "1");
            if(i==4 || i==5) {
                switcch.put(DIMMER_STATUS, "1");
            }else{
                switcch.put(DIMMER_STATUS, "0");
            }
            switcch.put(SWITCH_IMAGE_ID,arrHallSwitchImage[i]);
            switcch.put(DIMMER_VALUE, "0");
            switcch.put(LOCK, "0");
            switcch.put(HIDE, "0");
            db.insertSwitch(switcch);
        }

        HashMap<String, String> switcch1 = new HashMap<String, String>();

         String[] arrKitchenSwitch={"Purifier","Exhaust","Socket","Refrigerator","Microwave","Mixer"};
        String[] arrKitchenSwitchImage={"13","7","13","9","10","11"};

        for(int i=0;i<6;i++){
            switcch1.put(SWITCH_NAME,arrKitchenSwitch[i]);
            switcch1.put(ROOM_NAME, "Kitchen");
            switcch1.put(SWITCH_TYPE_ID, (i+1)+"");
            switcch1.put(SWITCH_STATUS, "0");
            switcch1.put(ROOM_ID, "2");
            if(i==4 || i==5) {
                switcch1.put(DIMMER_STATUS, "1");
            }else{
                switcch1.put(DIMMER_STATUS, "0");
            }
            switcch1.put(DIMMER_VALUE, "0");
            switcch1.put(SWITCH_IMAGE_ID,arrKitchenSwitchImage[i]);
            switcch1.put(LOCK, "0");
            switcch1.put(HIDE, "0");
            db.insertSwitch(switcch1);
        }

        HashMap<String, String> switcch2 = new HashMap<String, String>();
        String[] arrHallSwitch2={"Tube","Socket","Desk Lamp","AC","Dimmer Bulb","Fan"};
        String[] arrHallSwitchImage2={"18","13","4","1","0","8"};

        for(int i=0;i<6;i++){
            switcch2.put(SWITCH_NAME,arrHallSwitch2[i]);
            switcch2.put(ROOM_NAME, "Bedroom");
            switcch2.put(SWITCH_TYPE_ID, (i+1)+"");
            switcch2.put(SWITCH_STATUS, "0");
            switcch2.put(ROOM_ID, "3");
            if(i==4 || i==5) {
                switcch2.put(DIMMER_STATUS, "1");
            }else{
                switcch2.put(DIMMER_STATUS, "0");
            }
            switcch2.put(DIMMER_VALUE, "0");
            switcch2.put(SWITCH_IMAGE_ID,arrHallSwitchImage2[i]);
            switcch2.put(LOCK, "0");
            switcch2.put(HIDE, "0");
            db.insertSwitch(switcch2);
        }


        HashMap<String, String> switcch3 = new HashMap<String, String>();
        String[] arrHallSwitch3={"Tube","Dimmer Bulb"};
        String[] arrHallSwitchImage3={"18","0"};

        for(int i=0;i<2;i++){
            switcch3.put(SWITCH_NAME,arrHallSwitch3[i]);
            switcch3.put(ROOM_NAME, "Balcony");
            switcch3.put(SWITCH_TYPE_ID, (i+1)+"");
            switcch3.put(SWITCH_STATUS, "0");
            switcch3.put(ROOM_ID, "4");
            if(i==1 ) {
                switcch3.put(DIMMER_STATUS, "1");
            }else{
                switcch3.put(DIMMER_STATUS, "0");
            }
            switcch3.put(DIMMER_VALUE, "0");
            switcch3.put(SWITCH_IMAGE_ID,arrHallSwitchImage3[i]);
            switcch3.put(LOCK, "0");
            switcch3.put(HIDE, "0");
            db.insertSwitch(switcch3);
        }

        HashMap<String, String> switcch4 = new HashMap<String, String>();
        String[] arrHallSwitch4={"Washing Machine","Socket","Exhaust","Water Heater"};
        String[] arrHallSwitchImage4={"19","13","7","20"};

        for(int i=0;i<4;i++){
            switcch4.put(SWITCH_NAME,arrHallSwitch4[i]);
            switcch4.put(ROOM_NAME, "Bathroom");
            switcch4.put(SWITCH_TYPE_ID, (i+1)+"");
            switcch4.put(SWITCH_STATUS, "0");
            switcch4.put(ROOM_ID, "5");
            switcch4.put(DIMMER_STATUS,"0");
            switcch4.put(DIMMER_VALUE, "0");
            switcch4.put(SWITCH_IMAGE_ID,arrHallSwitchImage4[i]);
            switcch4.put(LOCK, "0");
            switcch4.put(HIDE, "0");
            db.insertSwitch(switcch4);
        }
    }
    private void insertRoom() {

        HashMap<String, String> room = new HashMap<String, String>();
        room.put(ROOM_NAME, "Hall");
        room.put(ROOM_TYPE_ID,"1");
        room.put(ROOM_IMAGE_TYPE,"1");
        room.put(ROOM_ID,"1");
        db.insertRoom(room);

        HashMap<String, String> room1 = new HashMap<String, String>();
        room1.put(ROOM_NAME, "Kitchen");
        room1.put(ROOM_TYPE_ID, "2");
        room1.put(ROOM_IMAGE_TYPE,"2");
        room1.put(ROOM_ID,"2");
        db.insertRoom(room1);

        HashMap<String, String> room2 = new HashMap<String, String>();
        room2.put(ROOM_NAME, "Bedroom");
        room2.put(ROOM_TYPE_ID, "3");
        room2.put(ROOM_IMAGE_TYPE, "5");
        room2.put(ROOM_ID,"3");
        db.insertRoom(room2);

        HashMap<String, String> room3 = new HashMap<String, String>();
        room3.put(ROOM_NAME, "Balcony");
        room3.put(ROOM_TYPE_ID, "4");
        room3.put(ROOM_IMAGE_TYPE, "4");
        room3.put(ROOM_ID,"4");
        db.insertRoom(room3);

        HashMap<String, String> room4 = new HashMap<String, String>();
        room4.put(ROOM_NAME, "Bathroom");
        room4.put(ROOM_TYPE_ID, "5");
        room4.put(ROOM_IMAGE_TYPE, "3");
        room4.put(ROOM_ID,"5");
        db.insertRoom(room4);
    }
    private void getRoomListServer() throws IOException, JSONException {

//

        if (URL_GENIE.equals(URL_GENIE_AWS)) {
            messageType = INTERNET;
        } else {
            messageType = LOCAL_HUB;


        }
        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("userId", sessionManager.getUSERID());
        jsonParams.put("messageFrom", messageType);
        strWebserviceName = "/room/getlistbyuser";
        methodType = Request.Method.POST;
        //callWebService(methodType, jsonParams, strWebserviceName);

        //setRoomAdapter();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new AsyncRoomListTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            } else {
                new AsyncRoomListTask().execute();

            }


    }

    class AsyncRoomListTask extends AsyncTask<Void,Void,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //pDialog = new ProgressDialog(getActivity());
            //pDialog.setMessage("Please Wait...");
            //pDialog.setCancelable(false);
           // pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            Constants request=new Constants();
            String mResponse=null;
            try {

                JSONObject jMain=new JSONObject();

                String token =sessionManager.getSecurityToken();
                Log.d("Room Url",sessionManager.getSecurityToken());
                jMain.put("userId",sessionManager.getUSERID());
                jMain.put("messageFrom",messageType);   //INTERNET //messageType



               // mResponse=request.doPostRoomRequest(URL_GENIE+"/room/getlistbyuser",jMain+"",sessionManager.getSecurityToken());
                mResponse=request.doPostRoomRequest(URL_GENIE_AWS+"/room/getlistbyuser",jMain+"",sessionManager.getSecurityToken());
                String url =URL_GENIE_AWS+"/room/getlistbyuser";                   //url_genie original
                Log.d("Get_room_response","url val ="+url);
                Log.d("Get_room_response","json val ="+jMain);
                Log.d("Get_room_response","response val ="+mResponse);

                Log.d("room_api_call","api call");


            }catch(Exception e){}

            return mResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            if(result!=null){
                Log.d("Get_room_response : ",result+"");
                try {


                    JSONObject response=new JSONObject(result);
                    String status = response.getString("status");
                    String results = response.getString("result");
                    JSONArray resultArray = new JSONArray(results);

                    if (status.equals("SUCCESS")) {

                        createListForRoom(resultArray);
                        getActivity().startService(new Intent(getActivity(),GetSwitchByRoomService.class));

                    }else{
                        Toast.makeText(context, ""+response.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }else{
            }

            setRoomAdapter();
        }
    }
    private void createListForRoom(JSONArray resultArray) {

        roomDatas.clear();
        for (int i = 0; i < resultArray.length(); i++) {

            HashMap<String, String> roomType = new HashMap<String, String>();
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(resultArray.get(i).toString());

                roomType.put(ROOM_ID, jsonObject.getString("id"));
                roomType.put(ROOM_NAME, jsonObject.getString("roomName"));
                roomType.put(ROOM_TYPE_ID, jsonObject.getString("roomType"));
                roomType.put(ROOM_IMAGE_TYPE, jsonObject.getString("roomImage")); //roomImageId
                roomDatas.add(roomType);
                db.insertRoomNew(roomType);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }
    private void insertRoomType() {

        HashMap<String, String> roomtype = new HashMap<String, String>();

        roomtype.put("roomTypeName", "Hall");
        db.insertRoomType(roomtype);

        roomtype.put("roomTypeName", "Kitchen");
        db.insertRoomType(roomtype);


        roomtype.put("roomTypeName", "Bedroom");
        db.insertRoomType(roomtype);

        roomtype.put("roomTypeName", "Toliet");
        db.insertRoomType(roomtype);


    }
    private void insertSwitchType() {

        HashMap<String, String> type = new HashMap<String, String>();

        type.put("switchTypeName", "Lamp");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Fan");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Bulb");
        db.insertSwitchType(type);

        type.put("switchTypeName", "CFL Bulb");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Electric Stove");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Fridge");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Geyser");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Microwave");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Mosquito Machine");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Wall Socket");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Curtains");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Iron machine");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Washing Machine");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Air Conditioner");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Other");
        db.insertSwitchType(type);
    }
    public void onResume() {
        super.onResume();

        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_up);
        bottomUp.setDuration(500);
        lstRoom.startAnimation(bottomUp);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(message, new IntentFilter("NotificationSend"));
        Log.d("mqtt_msg","data="+message.toString());
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(message, new IntentFilter("INTERNET_TEST"));


       setRoomAdapter();


     /*   if (sessionManager.getUSERID().equals("")) {
            getRoomListLocal();
            setRoomAdapter();

        }
        else
        {
            if (isInternetAvailable(getActivity()))
                {
                    try {
                        try {
                            getRoomListServer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            else
                {
                setRoomAdapter();
                myToast(getActivity(), MESSAGE_INTERNET_CONNECTION);
                }
        }*/

    }

    private BroadcastReceiver message = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            setRoomAdapter();


        }
    };

    private void setRoomAdapter() {
        try {
            roomDatas = db.getRooms();
            if (roomDatas.size() > 0) {
                tvMessageInt.setVisibility(View.GONE);
                lstRoom.setVisibility(View.VISIBLE);
                adapter = new AdapterCustomRooms(getActivity(), roomDatas);

                lstRoom.setAdapter(adapter);
                lstRoom.setLayoutManager(new LinearLayoutManager(getActivity()));
                adapter.notifyDataSetChanged();

            } else {
                lstRoom.setVisibility(View.GONE);
                tvMessageInt.setVisibility(View.VISIBLE);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            Log.d("eror_val",""+e.getMessage());
        }
    }

    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.edit:
                if(sessionManager.getUserType().equals("2")) {
                    Toast.makeText(context,getResources().getString(R.string.MSG_USER_AUTH),Toast.LENGTH_SHORT).show();
                }
                else {
                    addRoomDialog1("Edit", adapter.getPosition() + "");

                }
                break;
            case R.id.turoffroom:
                //alertTurnOffAllSwitch(roomDatas.get(adapter.getPosition()).get(ROOM_ID),"0");
                if(db.isAlreadyONOFF(roomDatas.get(adapter.getPosition()).get(ROOM_ID),"0")) {
                    new TurnOffAllSwitchAsyncTask().execute("0", roomDatas.get(adapter.getPosition()).get(ROOM_ID));
                }else{
                    Toast.makeText(context, "Switches are already OFF", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.turnonroom:
                //alertTurnOffAllSwitch(roomDatas.get(adapter.getPosition()).get(ROOM_ID),"1");
                if(db.isAlreadyONOFF(roomDatas.get(adapter.getPosition()).get(ROOM_ID),"1")) {
                    new TurnOffAllSwitchAsyncTask().execute("1", roomDatas.get(adapter.getPosition()).get(ROOM_ID));
                }else{
                    Toast.makeText(context, "Switches are already ON", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.hide_room:
                alertHideSwitch();
                break;
            case R.id.hide:
                 createAlertPassdDialog();
                break;
            case R.id.menu_hide:
                 onClickOfHideMenu();
                break;
             default:
                return super.onContextItemSelected(item);
        }

        return true;
    }

    private void alertTurnOffAllSwitch(final String mRoomId, final String operation) {

        final Dialog dialog = new Dialog(getActivity());
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_turnoff_all);
        TextView txtOk = (TextView) dialog.findViewById(R.id.txtOk);
        TextView tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        TextView tvMessage1 = (TextView) dialog.findViewById(R.id.tvMessage1);

        TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        if(operation.equals("0")) {
            tvMessage.setText("Are you sure to turn OFF all appliances of room ?");
            tvMessage1.setText("Turn OFF All");
        }else{
            tvMessage.setText("Are you sure to turn ON all appliances of room ?");
            tvMessage1.setText("Turn ON All");
        }

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                //String roomid=mRoomId;
                new TurnOffAllSwitchAsyncTask().execute(operation,mRoomId);
                dialog.dismiss();

            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void alertHideSwitch()    {

        if(sessionManager.getUserType().equals("2"))
        {
            Toast.makeText(context, getResources().getString(R.string.MSG_USER_AUTH), Toast.LENGTH_SHORT).show();

        }
        else {
            final Dialog dialog = new Dialog(getActivity());
            // Include dialog.xml file
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_hide_switch);
            TextView txtOk = (TextView) dialog.findViewById(R.id.txtOk);
            TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
            txtOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    onClickOfHideMenu();
                }
            });
            txtCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    private void onClickOfHideMenu() {

        DatabaseHandler db=new DatabaseHandler(getActivity());
        db.updateRoomHidestatus(roomDatas.get(adapter.getPosition()).get(ROOM_ID),"1");
        db.changeRoomSwitchStatus(roomDatas.get(adapter.getPosition()).get(ROOM_ID),"0");
        setRoomAdapter();
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("NotificationSend"));

    }
    private void createAlertPassdDialog() {

        final Dialog dialog = new Dialog(getActivity());
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_ip_change);
        TextView txtOk = (TextView) dialog.findViewById(R.id.txtOk);
        TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        final EditText edtPassword = (EditText) dialog.findViewById(R.id.edtPassword);

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String edtPass=edtPassword.getText().toString();
                if(edtPass.equals("")){
                    Toast.makeText(context,getResources().getString(R.string.MSG_CORRECT_PASS), Toast.LENGTH_SHORT).show();
                    return;
                }

                if(edtPass.equals(Constants.CHANGEIP_PASS)){
                    dialog.dismiss();
                    Intent intent=new Intent(getActivity(),ChangeIPActivity.class);
                    intent.putExtra(ROOM_ID,roomDatas.get(adapter.getPosition()).get(ROOM_ID));
                    startActivity(intent);
                }else{
                    Toast.makeText(context,getResources().getString(R.string.MSG_CORRECT_PASS), Toast.LENGTH_SHORT).show();
                }


            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
    private void getRoomListLocal() {

        try {
            roomDatas.clear();
            roomDatas = db.getRooms();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void initWidgets()  {

        context = getActivity();
        roomDatas = new ArrayList<HashMap<String, String>>();
        sessionManager = new SessionManager(getActivity());
        txtCount = (TextView) view.findViewById(R.id.txtCount);
        tvMessageInt = (TextView) view.findViewById(R.id.tvMessage);

        //lstRooms = (ListView) view.findViewById(R.id.listView);
        lstRoom= (RecyclerView) view.findViewById(R.id.lstRoom);
        linearLayout= (LinearLayout) view.findViewById(R.id.linearLayout);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);

    }

    public void onClick(View v) {

        switch (v.getId()) {
           case R.id.turnOffHome:
                  onIntentSelection("0");
               break;
            case R.id.turnOnHome:
                  onIntentSelection("1");
                break;
        }
    }

    private void onIntentSelection(String action){
        Intent intentON = new Intent(getActivity(), SwitchSelectionActivity.class);
        intentON.putExtra("ACTION",action);
        startActivity(intentON);
        //fab.close(true);
    }

    private void addRoomDialog1(String operation, String index) {

        Intent intent = new Intent(getActivity(), AddRooms.class);
        intent.putExtra(OPERATION, operation);

        if (operation.equals("Edit")) {
            intent.putExtra(ROOM_ID, roomDatas.get(Integer.parseInt(index)).get(ROOM_ID));
            intent.putExtra(ROOM_TYPE_ID, roomDatas.get(Integer.parseInt(index)).get(ROOM_TYPE_ID));
            intent.putExtra(ROOM_NAME, roomDatas.get(Integer.parseInt(index)).get(ROOM_NAME));
            // intent.putExtra(ROOM_IMAGE_TYPE,roomDatas.get(Integer.parseInt(index)).get(ROOM_IMAGE_TYPE));
            intent.putExtra(ROOM_IMAGE_TYPE,"1");

        }
        getActivity().startActivity(intent);


    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedText = parent.getSelectedItem().toString();



        if (position != 0) {
            edtSwitchName.setText(selectedText);
            return;
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

//    @Override
//    public void onCreateOptionsMenu(
//            Menu menu, MenuInflater inflater) {inflater.inflate(R.menu.main_menu, menu);
//    }
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.action_hide:
//                Intent intent=new Intent(getActivity(), HideList.class);
//                intent.putExtra(ROOM_ID,"0");
//                intent.putExtra(ROOM_NAME,"0");
//                startActivity(intent);
//                break;
//            default:
//                break;
//        }
//        return true;
//    }

    private class TurnOffAllSwitchAsyncTask extends AsyncTask<String,Void,String>
    {
        String status="";
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
          Constants req=new Constants();
            HashMap<String, String> map = null;
            String dimmerValue="";
            String response=null;
            String mqttMessage=null;
             ArrayList<HashMap<String, String>> switchDatas;
            try {
                //Parameter (Switch status,mRoomId)
                status=params[0];
                String mRequest = createJSONBody(params[0],params[1]);
                String onoffstatus=params[0];
                String roomid=params[1];

              // response=  req.doPostRequest(URL_GENIE + "/switch/changeStatusByRoom", mRequest,sessionManager.getSecurityToken());

                setMqttClient();
                Log.d("publish_switch_all","roomid==="+roomid);

                switchDatas = db.getSwitches(params[1]);

                Log.d("publish_switch_all","switchlist==="+switchDatas);
                for(int i=0;i<=switchDatas.size();i++)
                {
                    if (switchDatas.get(i).get(DIMMER_STATUS).equals("1") && switchDatas.get(i).get(SWITCH_STATUS).equals("1")) {
                        if (Integer.parseInt(switchDatas.get(i).get(DIMMER_VALUE)) < 10) {

                            dimmerValue = "0" + switchDatas.get(i).get(DIMMER_VALUE);
                        } else {
                            dimmerValue = switchDatas.get(i).get(DIMMER_VALUE);

                        }
                       // mqttMessage = sessionManager.getUSERID()+"/"+switchDatas.get(i).get(SWITCH_ID)+"/"+switchDatas.get(i).get(SWITCH_STATUS)+"/"+dimmerValue+"/"+messageType;
                        mqttMessage = sessionManager.getUSERID()+"/"+switchDatas.get(i).get(SWITCH_ID)+"/"+onoffstatus+"/"+dimmerValue+"/"+messageType;

                    } else {

                       // mqttMessage = sessionManager.getUSERID()+"/"+switchDatas.get(i).get(SWITCH_ID)+"/"+switchDatas.get(i).get(SWITCH_STATUS)+"/"+"0"+"/"+messageType;
                        mqttMessage = sessionManager.getUSERID()+"/"+switchDatas.get(i).get(SWITCH_ID)+"/"+onoffstatus+"/"+"0"+"/"+messageType;
                        Log.d("switch_position ", mqttMessage);
                    }

                    MqttMessage message2 = new MqttMessage(mqttMessage.getBytes());
                    message2.setQos(0);
                    try {
                        if (mqqtClient != null) {

                            Log.d("publish_switch_all","on off msg===="+message2);
                            getMqttConnection().publish(topic, message2);
                            Log.d("publish_switch_all","===="+switchDatas.get(i).get(SWITCH_NAME));
                        }
                        else
                        {
                            setMqttClient();
                            getMqttConnection().publish(topic, message2);
                            Log.d("publish_switch_all","1===="+switchDatas.get(i).get(SWITCH_NAME));
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    db.updateSwitchStatus(switchDatas.get(i).get(SWITCH_ID), switchDatas.get(i).get(SWITCH_STATUS), switchDatas.get(i).get(DIMMER_VALUE));
                }


                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("NotificationSend"));




            } catch (Exception e)
              {e.printStackTrace();}

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            if(pDialog!=null && pDialog.isShowing()) {
                pDialog.dismiss();
            }

          /*  if (result != null && !result.isEmpty()) {
                Log.d("Living Turn All Room ", result);
                try {
                    JSONObject jResult = new JSONObject(result);
                    if (jResult.getString("status").equals("SUCCESS")) {
                        JSONArray jArrResult = new JSONArray(jResult.getString("result"));
                        for (int i = 0; i < jArrResult.length(); i++) {
                            JSONObject jSwitch = jArrResult.getJSONObject(i);

                            if(db.isDimmerSwitch(jSwitch.getString("siwtchid"))){
                                db.updateAllDimmerSwitchStatus(jSwitch.getString("siwtchid"), status);
                            }else {
                                db.updateAllSwitchStatus(jSwitch.getString("siwtchid"), status);
                            }
                        }

                        //Toast.makeText(context, "Action is done successfully.", Toast.LENGTH_SHORT).show();
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("NotificationSend"));
                    } else {
                        Toast.makeText(context, jResult.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                }
                setRoomAdapter();
            } else {
                Toast.makeText(context,getResources().getString(R.string.MSG_TRY_AGAIN), Toast.LENGTH_SHORT).show();
            }*/
        }
    }

    private String createJSONBody(String switchStatus,String mRoomId) {
        JSONObject jMain = new JSONObject();
        try {
            jMain.put("roomId", mRoomId);
            jMain.put("onoffstatus", switchStatus);
            jMain.put("userId",sessionManager.getUSERID());
            jMain.put("messageFrom",messageType);
        } catch (Exception e) {e.printStackTrace();}
        return jMain + "";
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(message);
    }

    private void setMqttClient(){
        try {

            broker = "tcp://"+sessionManager.getRouterIP()+":1883";
            Constants.URL_GENIE = "http://" + sessionManager.getRouterIP() + ":8080/smart_home_local";
            messageType=LOCAL_HUB;
            topic="refreshIntGenieHomeId_1";
            broker = "tcp://" + sessionManager.getRouterIP() + ":1883";


            Log.d("check_mqtt","Switch clicked inside1.3");
            clientId=System.currentTimeMillis()+"";
            persistence = new MemoryPersistence();
            mqqtClient = new MqttClient(broker, clientId, persistence);

            Log.d("client_id1","broker=="+broker);
            Log.d("client_id1","clientId=="+clientId);
            Log.d("client_id1","persistence=="+persistence);
            Log.d("client_id1","topic=="+topic);

            connOpts = new MqttConnectOptions();
            connOpts.setConnectionTimeout(300);
            connOpts.setCleanSession(true);


            Log.d("client_id1","connecting...");
            mqqtClient.connect(connOpts);
            Log.d("client_id1","connected");
            mqqtClient.setCallback((MqttCallback) this);

            mqqtClient.subscribe(topic);
            Log.d("client_id1","published");

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Exception -->",e.toString());
            Log.d("client_id1","mqtt connection error"+e.toString());
        }
    }
    protected MqttClient getMqttConnection() throws MqttException {
        if (mqqtClient != null) {
            System.out.println("reusing connection...");
            return mqqtClient;
        } else {
            Log.d("check_mqtt","Switch clicked inside1.2");
            setMqttClient();
            return mqqtClient;
        }
    }
}





