package Fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.genieiot.gsmarthome.LivingRoom;
import com.genieiot.gsmarthome.R;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import Adapter.AdapterCustomRecent;
import Database.DatabaseHandler;
import Session.Constants;
import Session.IOnClickOfSwitchChange;
import Session.NetworkConnectionInfo;
import Session.SessionManager;

import static Database.DatabaseHandler.DIMMER_STATUS;
import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.IP;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.SWITCH_TYPE_ID;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;

//import static com.genieiot.geniesmarthome.R.id.lstRecent;

/**
 * Created by Genie IoT on 9/13/2016.
 */
public class Recent extends Fragment implements MqttCallback, IOnClickOfSwitchChange {

    private View view;
    private RecyclerView lstSwitches;
    Context context;
    private String messageType="";
    String topic;

    //private AdapterRecent adapterSwitch;
    private AdapterCustomRecent adapterSwitch;
    private DatabaseHandler db;
    private ArrayList<HashMap<String, String>> listSwitchType;
    private ArrayList<HashMap<String, String>> switchDatas;

    int[] mSwitchON = new int[]{R.drawable.on_light_bulb,R.drawable.on_ac ,R.drawable.on_chandelier,R.drawable.on_cooler,R.drawable.on_desk_lamp,
            R.drawable.on_desktop,R.drawable.on_dish,R.drawable.on_exost,R.drawable.on_fan,R.drawable.on_refrigerator,
            R.drawable.on_microwave,R.drawable.on_mixer,R.drawable.on_purifier,R.drawable.on_socket,R.drawable.on_sound,
            R.drawable.on_stove,R.drawable.on_table_fan,R.drawable.on_television,R.drawable.on_tube,R.drawable.on_washing_machine,
            R.drawable.on_water_heater};


    int[] mSwitchOFF = new int[]{R.drawable.off_bulb,R.drawable.off_ac,R.drawable.off_chandelier,R.drawable.off_cooler,R.drawable.off_desk_lamp,
            R.drawable.off_desktop_computer, R.drawable.off_dish,R.drawable.off_exost,R.drawable.off_fan,R.drawable.off_fridge,
            R.drawable.off_microwave,R.drawable.off_mixer,R.drawable.off_purifier,R.drawable.off_socket,R.drawable.off_sound,
            R.drawable.off_stove,R.drawable.off_table_fan,R.drawable.off_television,R.drawable.off_tube,R.drawable.off_washing_machine,
            R.drawable.off_water_heater};

    ArrayList<HashMap<String,Integer>> mSwitchONOFF=new ArrayList<>();

    String broker = "";
    String clientId = "";
    MqttConnectOptions connOpts;
    MqttClient mqqtClient = null;
    MemoryPersistence persistence;
    SessionManager sessionManager;
    String mNetworkInfo;
    String type = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.recent, container, false);
        db = new DatabaseHandler(getActivity());
        initWidgets();
        sessionManager=new SessionManager(getActivity());
        broker="tcp://"+sessionManager.getRouterIP()+":1883";

        for(int i=0;i<mSwitchOFF.length;i++){
            HashMap<String,Integer> mMap=new HashMap<>();
            mMap.put("ON",mSwitchON[i]);
            mMap.put("OFF",mSwitchOFF[i]);
            mSwitchONOFF.add(mMap);

        }

        if(sessionManager.getDemoUser().equals("DemoUser")) {
            if (NetworkConnectionInfo.CheckWifiConnection(context)) {
                mNetworkInfo = "WIFI";
                broker = "tcp://" + sessionManager.getRouterIP() + ":1883";
            } else
            {//                mNetworkInfo = "INTERNET";
//                broker = "tcp://geniewish.genieiot.com:1883";

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                {
                    new AsyncTaskCheckRouter().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                else
                {
                    new AsyncTaskCheckRouter().execute();
                }
            }

        }
        setSwitchAdapter();
        return view;
    }

    private void initWidgets() {
        context = getActivity();
        lstSwitches= (RecyclerView) view.findViewById(R.id.lstSwitches);
       // lstRecent = (ListView) view.findViewById(lstRecent);
    }

    protected MqttClient getMqttConnection() throws MqttException {
        if (mqqtClient != null) {
            System.out.println("reusing conntion...");
            return mqqtClient;
        } else {
             new AsyncMqttClientTask().execute();
            return mqqtClient;
        }
    }

    @Override
    public void onResume() {

        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(messageRecent, new IntentFilter("MqttCallBack"));
     //   LocalBroadcastManager.getInstance(getActivity()).registerReceiver(messageActivity, new IntentFilter("NotificationSend"));
        //getSwitchesList();
        setSwitchAdapter();

      /*  if(sessionManager.getDemoUser().equals("DemoUser")) {
            if (NetworkConnectionInfo.CheckWifiConnection(context)) {
                mNetworkInfo = "WIFI";
                broker = "tcp://" + sessionManager.getRouterIP() + ":1883";
            } else
            {//                mNetworkInfo = "INTERNET";
//                broker = "tcp://geniewish.genieiot.com:1883";

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                {
               new AsyncTaskCheckRouter().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                else
                {
                new AsyncTaskCheckRouter().execute();
                }
            }

        }*/
       // setMqttClient();
    }


    class AsyncMqttClientTask extends AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... params) {
            setMqttClient();
            return null;
        }
    }

    private void setMqttClient() {

        try {
            Log.d("check_mqtt","Switch clicked inside1.3");
            //
            // mqqtClient.connect(connOpts);
            //Thread.sleep(6000);
            //   new AsynOperation().execute();
            //      mqqtClient.subscribe(topic);
            //  Log.d("Broker",broker);


            //    Constants.URL_GENIE = Constants.URL_GENIE_AWS;


            clientId=System.currentTimeMillis()+"";
            persistence = new MemoryPersistence();
            mqqtClient = new MqttClient(broker, clientId, persistence);

            Log.d("client_id1","broker=="+broker);
            Log.d("client_id1","clientId=="+clientId);
            Log.d("client_id1","persistence=="+persistence);
            Log.d("client_id1","topic=="+topic);

            connOpts = new MqttConnectOptions();
            connOpts.setConnectionTimeout(300);
            connOpts.setCleanSession(true);


            Log.d("client_id1","connecting...");
            mqqtClient.connect(connOpts);
            Log.d("client_id1","connected");
            mqqtClient.setCallback(Recent.this);

            mqqtClient.subscribe(topic);
            Log.d("client_id1","published");

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Exception -->",e.toString());
            Log.d("client_id1","mqtt connection error"+e.toString());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(messageRecent);
    }

    public void getSwitchesList() {

        try {
            switchDatas = new ArrayList<HashMap<String,String>>();
            switchDatas.clear();
            switchDatas = db.getRecentSwitches();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void setSwitchAdapter()
    {
        try {
            switchDatas = new ArrayList<>();
            switchDatas.clear();
            switchDatas = db.getRecentSwitches();
            Collections.reverse(switchDatas);
            adapterSwitch = new AdapterCustomRecent(getActivity(), switchDatas, mSwitchONOFF, Recent.this);
            lstSwitches.setLayoutManager(new LinearLayoutManager(context));
            lstSwitches.setAdapter(adapterSwitch);
            adapterSwitch.notifyDataSetChanged();
        }catch(Exception e){}

    }

    private BroadcastReceiver messageRecent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            //setActivityCount();
            setSwitchAdapter();

        }
    };

//    private BroadcastReceiver messageActivity = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            // TODO Auto-generated method stub
//
//            if(adapterSwitch != null){
//
//                switchDatas = db.getRecentSwitches();
//                adapterSwitch.updateData(switchDatas);
//                adapterSwitch.notifyDataSetChanged();
//               // setSwitchAdapter();
//            }
//        }
//    };


    @Override
    public void connectionLost(Throwable throwable) {

    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        Log.d("Recent-->","MessageArrived()");
        Log.d("Recent-->",s);
        Log.d("Recent-->",mqttMessage.toString());
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    @Override
    public void OnClickOfSwitchChange(HashMap<String, String> map) {
        String mqttMessage=null;
        try {
            mqttMessage = "$[" + map.get(SWITCH_TYPE_ID) + 0 + map.get(SWITCH_STATUS)+"0]";
            Log.d("Living ROom ",mqttMessage);

            String topic="";

            if(mNetworkInfo.equals("INTERNET")){
              topic=sessionManager.getTopicName();
                mqttMessage="wish,"+map.get(IP)+"/out,"+mqttMessage;
            }else{
                topic=map.get(IP)+"/out";
               // String topicIn=map.get(IP)+"/in";
            }

            MqttMessage message2 = new MqttMessage(mqttMessage.getBytes());
            message2.setQos(2);
            Log.d("Message ",mqttMessage);
            Log.d("Topic -->",topic);
            Log.d("Broker ",broker);
            if(mqqtClient!=null) {
                getMqttConnection().publish(topic, message2);
            }else{
                new AsyncMqttClientTask().execute();
            }

            //getMqttConnection().subscribe(topic);
            System.out.println("Message published");
            db.updateSwitchStatus(map.get(SWITCH_ID),map.get(SWITCH_STATUS),map.get(DIMMER_VALUE));
        } catch (MqttException e) {
            e.printStackTrace();
        }

        adapterSwitch.notifyDataSetChanged();
    }




    public void OnClickOfRecentSwitchChange(HashMap<String, String> map) {

        String mqttMessage=null;
        String dimmerValue="";
        try {
            if (map.get(DIMMER_STATUS).equals("1") && map.get(SWITCH_STATUS).equals("1")) {
                if (Integer.parseInt(map.get(DIMMER_VALUE)) < 10) {

                    dimmerValue = "0" + map.get(DIMMER_VALUE);
                } else {
                    dimmerValue = map.get(DIMMER_VALUE);

                }

                mqttMessage = sessionManager.getUSERID()+"/"+map.get(SWITCH_ID)+"/"+map.get(SWITCH_STATUS)+"/"+dimmerValue+"/"+messageType;

            } else {

                mqttMessage = sessionManager.getUSERID()+"/"+map.get(SWITCH_ID)+"/"+map.get(SWITCH_STATUS)+"/"+"0"+"/"+messageType;
                Log.d("switch_position ", mqttMessage);
            }






//            if(mNetworkInfo.equals("INTERNET")){
//                topic=sessionManager.getTopicName();
//                mqttMessage="wish,"+map.get(IP)+"/out,"+mqttMessage;
//            }else{
//                topic=map.get(IP)+"/out";
//                // String topicIn=map.get(IP)+"/in";
//            }




            if(messageType.equals("INTERNET")){
                topic="refreshIntGenieHomeId_1";
                // mqttMessage="wish,"+getRoomIP()+"/out,"+mqttMessage;
                mqttMessage= "wish/Internet_Switch_On_Off/"+mqttMessage;

                Log.d("Living_ROom ", "mqttMessage==="+mqttMessage);
                Log.d("Living_ROom","topic=="+topic);
                Log.d("Living_ROom","broker=="+broker);

                MqttMessage message2 = new MqttMessage(mqttMessage.getBytes());
                message2.setQos(0);
                try {
                    if (mqqtClient != null) {

                        getMqttConnection().publish(topic, message2);
                    }
                    else
                    {
                        setMqttClient();
                        getMqttConnection().publish(topic, message2);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }

            }
            else {
                topic="refreshIntGenieHomeId_1";
                mqttMessage= "wish/Local_Switch_On_Off/"+mqttMessage;
                Log.d("Living_ROom1", "mqttMessage"+mqttMessage);
                Log.d("Living_ROom1","Switch clicked inside1"+mqttMessage +"topic="+topic);


                MqttMessage message2 = new MqttMessage(mqttMessage.getBytes());
                message2.setQos(0);

                // setMqttClient();
                if (mqqtClient != null) {

                    Log.d("Living_ROom1 ", mqttMessage);
                    Log.d("Living_ROom1",topic);

                    getMqttConnection().publish(topic, message2);

                }else{

                    setMqttClient();
                    getMqttConnection().publish(topic, message2);
                    //   getMqttConnection().publish(topic, message2);

                }
                //new AsynOperation().execute();

            }

            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("NotificationSend"));

            System.out.println("Message published");
            db.updateSwitchStatus(map.get(SWITCH_ID),map.get(SWITCH_STATUS),map.get(DIMMER_VALUE));
            db.updateRecentSwitchStatus(map.get(SWITCH_ID),map.get(SWITCH_STATUS),map.get(DIMMER_VALUE));

        } catch (MqttException e) {
            e.printStackTrace();
        }

        adapterSwitch.notifyDataSetChanged();
    }

    @Override
    public void OnProgressChangeListener(HashMap<String, String> map) {

    }
    private class AsyncTaskCheckRouter extends AsyncTask<Void, Void, String> {
        private ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Checking Hub..");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(Void... voids) {

            InetAddress routername = null;

            Log.d("mqtt_checking","checked");
            try {
                routername = InetAddress.getByName(sessionManager.getRouterIP());
                if (routername.isReachable(5000))
                {
                    Log.d("network_checking1","Router is   reachable");
                    type=LOCAL_HUB;
                }
                else
                {
                    Log.d("network_checking1","Router is not  reachable");
                    type=INTERNET;
                }

            } catch (java.io.IOException e) {
                e.printStackTrace();
            }

            return type;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();

            }

            if(result.equals(LOCAL_HUB))
            {

                Log.d("mqtt_case_check","On off page wifi");

                broker = "tcp://"+sessionManager.getRouterIP()+":1883";
                Constants.URL_GENIE = "http://" + sessionManager.getRouterIP() + ":8080/smart_home_local";
                messageType=LOCAL_HUB;
                Log.d("check_mqtt","On off page wifi");
                topic="refreshIntGenieHomeId_1";
                broker = "tcp://" + sessionManager.getRouterIP() + ":1883";



            }
            else
            {

                if(NetworkConnectionInfo.isInternetAvailable(context)) {

                    Log.d("mqtt_case_check","On off page internet");

                    //broker = "tcp://103.12.211.52:1883";
                    broker = "tcp://192.168.0.2:1883";
                    Constants.URL_GENIE = Constants.URL_GENIE_AWS;
                    messageType=INTERNET;
                    topic="refreshIntGenieHomeId_1";

                    Log.d("check_mqtt","On off page internet");

                }
                else{
                    Toast.makeText(context, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }
}